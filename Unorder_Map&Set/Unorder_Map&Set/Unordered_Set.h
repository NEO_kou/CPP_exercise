#pragma once
#include"HashTabal.h"

template<class K, class Hash = HashFunc<K>>
class Unordered_Set
{
	struct SKOT
	{
		const K& operator()(const K& kv)
		{
			return kv;
		}
	};
public:
	typedef typename OpenedHash::HashTable<K, K, Hash, SKOT>::iterator iterator;

	iterator begin()
	{
		return _ht.begin();
	}
	iterator end()
	{
		return _ht.end();
	}
	pair<iterator, bool> insert(const K& key)
	{
		return _ht.insert(key);
	}

private:
	OpenedHash::HashTable<K, K, Hash, SKOT> _ht;
};

void testset1()
{
	Unordered_Set<int, HashFunc<int>> s;
	//set<int> s;
	s.insert(1);
	s.insert(2);
	s.insert(3);
	s.insert(4);
	s.insert(5);
	auto it = s.begin();
	while (it != s.end())
	{
		cout << *it << " ";
		++it;
	}
	cout << endl;
}