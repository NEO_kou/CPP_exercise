#pragma once
#include<vector>
#include<iostream>
#include<map>
#include<string>
using namespace std;

template<class T>
struct HashFunc//作为仿函数的缺省类型
{
	size_t operator()(const T& key)
	{
		return key;
	}
};

template<>
struct HashFunc<string>
{
	//BKDR算法:将字符串转换为整数
	size_t operator()(const string& str)
	{
		size_t sum = 0;
		for (auto ch : str)
		{
			sum *= 131;
			sum += (size_t)ch;
		}

		return sum;//将字符的asc码全部加起来再返回
	}
};

namespace ClosedHash  //哈希表的闭散列
{
	enum State
	{
		EMPTY,
		EXIST,
		DELETE
	};

	template<class K, class V>
	struct HashData
	{
		pair<K, V> _kv;
		State _state;
		HashData(const pair<K, V>& kv = make_pair(0, 0))
			:_kv(kv)
			, _state(EMPTY)
		{ }
	};

	template<class K, class V, class Hash = HashFunc<K>>
	class HashTable
	{
	public:
		bool insert(const pair<K, V>& kv)
		{
			if (_table.size() == 0 || 10 * _size / _table.size() >= 7) // 扩容
			{
				size_t newSize = _table.size() == 0 ? 10 : _table.size() * 2;
				HashTable<K, V, Hash> newHT;
				newHT._table.resize(newSize);
				// 旧表的数据映射到新表
				for (auto e : _table)
				{
					if (e._state == EXIST)
					{
						newHT.insert(e._kv);
					}
				}
				_table.swap(newHT._table);
			}
			Hash hash;
			size_t index = hash(kv.first) % _table.size();//不能模capacity,如果模出来的数大于size了还插入进去了会报错
			//线性探测
			while (_table[index]._state == EXIST)
			{
				index++;
				index %= _table.size();//过大会重新回到起点
			}
			_table[index]._kv = kv;
			_table[index]._state = EXIST;
			_size++;
			return true;
		}

		HashData<K, V>* find(const K& key)
		{
			if (_table.size() == 0)
				return nullptr;
			Hash hash;
			size_t index = hash(key) % _table.size();//负数会提升成无符号数,所以负数不影响结果,但是string类不能取模,需要加入一个仿函数
			size_t start = index;
			while (_table[index]._state != EMPTY)
			{
				if (_table[index]._kv.first == key && _table[index]._state == EXIST)
					return &_table[index];
				index++;
				index %= _table.size();
				if (index == start)//全是DELETE时,必要时会break
					break;
			}
			return nullptr;
		}

		bool erase(const K& key)
		{
			HashData<K, V>* ret = find(key);
			if (ret)
			{
				ret->_state = DELETE;
				--_size;
				return true;
			}
			return false;
		}

		void print()
		{
			for (int i = 0; i < _table.size(); i++)
				if (_table[i]._state == EXIST)
					printf("[%d]:%d ", i, _table[i]._kv.first);
		}

	private:
		//vector<pair<K, V>> _table;
		vector<HashData<K, V>> _table;
		size_t _size = 0; //有效数据的个数
	};

	void test1()
	{
		int a[] = { 1,11,4,15,26,7,44,9 };
		HashTable<int, int> ht;
		for (auto x : a)
			ht.insert(make_pair(x, x));
		ht.print();
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

namespace OpenedHash//哈希的开散列(也叫哈希桶)
{
	template<class T>
	struct HashNode
	{
		T _data;
		HashNode<T>* _next;//以单链表的方式链接
		HashNode(const T& data = T())
			:_data(data)
			,_next(nullptr)
		{ }
	};

	//为了给迭代器做前置声明(迭代器中用到了哈希表)
	template<class K, class T, class Hash, class KOT>
	class HashTable;

	template<class K, class T, class Hash, class KOT>
	struct __Hash_Iterator
	{
		friend HashTable<K, T, Hash, KOT>;
		typedef HashNode<T> Node;
		typedef HashTable<K, T, Hash, KOT> HT;
		typedef __Hash_Iterator<K, T, Hash, KOT> Self;
		Node* _node;
		HT* _pht;//需要一个哈希表
		__Hash_Iterator(Node* node, HT* pht)
			:_node(node)
			,_pht(pht)
		{ }

		T& operator*()
		{
			return _node->_data;
		}

		T* operator->()
		{
			return &(_node->_data);
		}

		Self& operator++()
		{
			if (_node->_next)
				_node = _node->_next;
			else//当前桶为空,找下一个桶
			{
				Hash hash;
				KOT kot;
				size_t i = hash(kot(_node->_data)) % _pht->_table.size();//找到当前在哪一个桶
				i++;
				for (; i < _pht->_table.size(); i++)
				{
					if (_pht->_table[i] != nullptr)
					{
						_node = _pht->_table[i];
						break;
					}	
				}
				if (i == _pht->_table.size())//说明后面没有有数据的桶的,返回空
					_node = nullptr;
			}
			return *this;
		}

		bool operator!=(const Self& s) const
		{
			return _node != s._node;
		}

		bool operator==(const Self& s) const
		{
			return _node == s._node;
		}
	};

	template<class K,class T, class Hash, class KOT>
	class HashTable
	{
		friend __Hash_Iterator<K, T, Hash, KOT>;
		typedef HashNode<T> Node;
	public:
		typedef __Hash_Iterator<K, T, Hash, KOT> iterator;

		iterator begin()
		{
			for (int i = 0; i < _table.size(); i++)
				if (_table[i] != nullptr)
					return iterator(_table[i], this);
			return end();
		}

		iterator end()
		{
			return iterator(nullptr, this);
		}

		~HashTable()
		{
			for (size_t i = 0; i < _table.size(); ++i)
			{
				Node* cur = _table[i];
				while (cur)
				{
					Node* next = cur->_next;
					delete cur;
					cur = next;
				}
				_table[i] = nullptr;
			}
		}

		inline size_t __stl_next_prime(size_t n)//哈希表的大小是素数时,效率会更好,并且每次扩容要接近两倍
		{
			static const size_t __stl_num_primes = 28;
			static const size_t __stl_prime_list[__stl_num_primes] =
			{
				53, 97, 193, 389, 769,
				1543, 3079, 6151, 12289, 24593,
				49157, 98317, 196613, 393241, 786433,
				1572869, 3145739, 6291469, 12582917, 25165843,
				50331653, 100663319, 201326611, 402653189, 805306457,
				1610612741, 3221225473, 4294967291
			};

			for (size_t i = 0; i < __stl_num_primes; ++i)
			{
				if (__stl_prime_list[i] > n)
				{
					return __stl_prime_list[i];
				}
			}
			return -1;
		}

		pair<iterator,bool> insert(const T& data)
		{
			Hash hash;
			KOT kot;
			//去重+扩容
			iterator ret = find(kot(data));
			if (ret != end())//若data已经存在,返回这个已存在的值的迭代器
				return make_pair(ret, false);
			//负载因子到1就扩容
			if (_size == _table.size())
			{
				vector<Node*> newT;
				//size_t newSize = _table.size() == 0 ? 10 : _table.size() * 2;
				//newT.resize(newSize, nullptr);
				newT.resize(__stl_next_prime(_table.size()), nullptr);
				//将旧表中的节点移动到新表
				for (int i = 0; i < _table.size(); i++)
				{
					Node* cur = _table[i];
					while (cur)
					{
						Node* next = cur->_next;
						size_t hashi = hash(kot(cur->_data)) % newT.size();
						cur->_next = newT[hashi];
						newT[i] = cur;
						cur = next;
					}
					_table[i] == nullptr;
				}
				_table.swap(newT);
			}
			size_t hashi = hash(kot(data)) % _table.size();
			//头插
			Node* newnode = new Node(data);
			newnode->_next = _table[hashi];
			_table[hashi] = newnode;
			++_size;
			return make_pair(iterator(newnode, this), true);
		}

		iterator find(const K& key)
		{
			Hash hash;
			KOT kot;
			if (_table.size() == 0)
				return end();
			size_t hashi = hash(key) % _table.size();
			Node* cur = _table[hashi];
			while (cur)
			{
				if (kot(cur->_data) == key)
					return iterator(cur, this);
				cur = cur->_next;
			}
			return end();
		}

		bool erase(const K& key)
		{
			Hash hash;
			KOT kot;
			Node* ret = find(key);
			if (ret == nullptr)
				return false;
			size_t hashi = hash(key) % _table.size();
			Node* cur = _table[hashi];
			Node* prev = nullptr;
			while (cur && kot(cur->_data) != key)
			{
				prev = cur;
				cur = cur->_next;
			}
			Node* next = cur->_next;
			if (cur == _table[hashi])
				_table[hashi] = next;
			else
				prev->_next = next;
			delete cur;
			cur = nullptr;
			_size--;
			return true;
		}

		size_t size()
		{
			return _size;
		}

		size_t bucketnum()//桶的数量
		{
			int num = 0;
			for (int i = 0; i < _table.size(); i++)
			{
				if (_table[i])
					++num;
			}
			return num;
		}

		size_t bucketsize()//表的实际长度
		{
			return _table.size();
		}
	private:
		vector<Node*> _table;
		size_t _size = 0;//有效数据个数
	};

	//void TestHT1()
	//{
	//	int a[] = { 1, 11, 4, 15, 26, 7, 44,55,99,78 };
	//	HashTable<int, int> ht;
	//	for (auto e : a)
	//	{
	//		ht.insert(make_pair(e, e));
	//	}
	//	ht.insert(make_pair(22, 22));//这次增容将旧表数据移动至新表
	//}

	//void test11()
	//{
	//	string arr[] = { "苹果", "西瓜", "苹果", "西瓜", "苹果", "苹果", "西瓜", "苹果", "香蕉", "苹果", "香蕉" };
	//	HashTable<string, int> countHT;
	//	for (auto& str : arr)
	//	{
	//		auto ptr = countHT.find(str);
	//		if (ptr)
	//		{
	//			ptr->_data.second++;
	//		}
	//		else
	//		{
	//			countHT.insert(make_pair(str, 1));
	//		}
	//	}
	//}

	//void test3()
	//{
	//	int n = 100000;
	//	vector<int> v;
	//	v.reserve(n);
	//	srand(time(0));
	//	for (int i = 0; i < n; ++i)
	//	{
	//		//v.push_back(i);
	//		v.push_back(rand() + i);  // 重复少
	//		//v.push_back(rand());  // 重复多
	//	}

	//	size_t begin1 = clock();
	//	HashTable<int, int> ht;
	//	for (auto e : v)
	//	{
	//		ht.insert(make_pair(e, e));
	//	}
	//	size_t end1 = clock();

	//	cout << "数据个数:" << ht.size() << endl;
	//	//cout << "表的长度:" << ht.TablesSize() << endl;
	//	cout << "桶的个数:" << ht.bucketnum() << endl;
	//	cout << "平均每个桶的长度:" << (double)ht.size() / (double)ht.bucketnum() << endl;
	//	//cout << "最长的桶的长度:" << ht.MaxBucketLenth() << endl;
	//	//cout << "负载因子:" << (double)ht.size() / (double)ht.TablesSize() << endl;
	//}
}