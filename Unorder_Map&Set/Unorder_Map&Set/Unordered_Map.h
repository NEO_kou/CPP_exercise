#pragma once
#include"HashTabal.h"

template<class K, class V, class Hash = HashFunc<K>>
class Unordered_Map
{
	struct MKOT
	{
		const K& operator()(const pair<K, V>& kv)
		{
			return kv.first;
		}
	};
public:
	typedef typename OpenedHash::HashTable<K, pair<K, V>, Hash, MKOT>::iterator iterator;
	iterator begin()
	{
		return _ht.begin();
	}
	iterator end()
	{
		return _ht.end();
	}

	pair<iterator, bool> insert(const pair<K, V>& kv)
	{
		return _ht.insert(kv);
	}

	V& operator[](const K& key)
	{
		pair<iterator, bool> ret = _ht.insert(make_pair(key, V()));
		return ret.first->second;
	}
private:
	OpenedHash::HashTable<K, pair<K, V>, Hash, MKOT> _ht;
};

void testmap1()
{
	Unordered_Map<int,int,HashFunc<int>> s;
	//set<int> s;
	s.insert(make_pair(1, 1));
	s.insert(make_pair(2, 2));
	s.insert(make_pair(3, 3));
	s.insert(make_pair(4, 4));
	s.insert(make_pair(5, 5));
	auto it = s.begin();
	while (it != s.end())
	{
		cout << it->first << ":" << it->second << " ";
		++it;
	}
	cout << endl;
}