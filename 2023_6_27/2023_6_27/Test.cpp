#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable:6031)

#include<iostream>
using namespace std;

//class NEO
//{
//public:
//	explicit NEO(int height)//加上explicit后,N2=173就不可用了
//		:_height(height)
//		, _weight(70)
//		, _year(21)
//		,_name("kwy")
//		,_hheight(_height)
//	{
//		_hheight++;
//	}
//
//	int reval(int x)
//	{
//		return x;
//	}
//
//	static int n;
//	static int Add(int x = 1, int y = 1);
//
//private:
//	int _height;
//	int _weight;
//	int _year;
//	const char _name[10];//const 变量必须在初始化列表初始化
//	int& _hheight;
//	//static int n;  //将静态成员定义在private中无法从外部访问,静态成员受private限制
//};
//
//
//int NEO::n = 10;//static类型的对象必须在类外定义
//
//int NEO::Add(int x, int y)
//{
//	//_height = 173;  //类中的静态成员没有this指针,不能访问如何非静态成员
//	return x + y;
//}
//
//class A
//{
//	friend int retuanA(int x,int y,A& aa);
//public:
//	A(int a = 10, int b = 20)
//		:_a(a)
//		,_b(b)
//	{
//		_a++;
//		_b--;
//	}
//
//private:
//	int _a;
//	int _b;
//};
//
//int retuanA(int x, int y,A& aa)//友元函数不能直接写_a,因为它不是类的内部函数,没有this指针
//{
//	 aa._a = x + y;
//	 aa._b = x - y;
//	 return aa._a;
//}
//
//void Test1()
//{
//	NEO N1(173);
//	//NEO N2 = 173;
//	int p = NEO(173).reval(10);//匿名对象
//	cout << p << endl;
//
//	//静态成员的三种访问方式
//	cout << NEO::n << endl;//若静态成员声明为private,则不能访问
//	cout << N1.n << endl;  //通过定义的变量访问
//	cout << NEO(173).n << endl;//通过匿名对象访问
//}
//
//
//int main()
//{
//	Test1();
//
//	return 0;
//}


class A
{
public:
	A(int a = 0)//构造
		:_a(a)
	{
		cout << "A(int a)" << endl;
	}
	A(const A& aa)//拷贝构造
		:_a(aa._a)
	{
		cout << "A(const A& aa)" << endl;
	}
		A& operator=(const A& aa)
	{
		cout << "A& operator=(const A& aa)" << endl;
		if (this != &aa)
		{
			_a = aa._a;
		}
		return *this;
	}
	~A()//析构
	{
		cout << "~A()" << endl;
	}
private:
	int _a;
};
void f1(A aa)
{}
A f2()
{
	A aa;
	return aa;
}
int main()
{
	// 传值传参
	A aa1;
	f1(aa1);
	cout << endl;
	// 传值返回
	f2();
	cout << endl;
	// 隐式类型，连续构造+拷贝构造->优化为直接构造
	f1(1);
	// 一个表达式中，连续构造+拷贝构造->优化为一个构造
	f1(A(2));
	cout << endl;
	// 一个表达式中，连续拷贝构造+拷贝构造->优化一个拷贝构造
	A aa2 = f2();
	cout << endl;
	// 一个表达式中，连续拷贝构造+赋值重载->无法优化
	aa1 = f2();
	cout << endl;
	return 0;
}