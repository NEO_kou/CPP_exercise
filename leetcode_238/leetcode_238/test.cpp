class Solution {
public:
    vector<int> productExceptSelf(vector<int>& nums) {
        int n = nums.size();
        vector<int> f(n);
        vector<int> g(n);
        f[0] = nums[0];
        g[n - 1] = nums[n - 1];
        for (int i = 1; i < n; i++)
            f[i] = f[i - 1] * nums[i];
        for (int i = n - 2; i >= 0; i--)
            g[i] = g[i + 1] * nums[i];
        vector<int> ret(n, 1);
        for (int i = 0; i < n; i++)
        {
            if (i > 0)
                ret[i] *= f[i - 1];
            if (i < n - 1)
                ret[i] *= g[i + 1];
        }
        return ret;
    }
};