#pragma once
#include<vector>
#include<iostream>
#include<math.h>
using namespace std;

template<size_t N>//N是所有数中的最大值
class bit_set
{
public:
	bit_set()
	{
		_bit.resize(N / 8 + 1, 0);
	}
	void set(size_t x)//将第x位变成1
	{
		//x/8->在第几个char
		//x%8->在这个char的第几个比特位
		size_t i = x / 8;
		size_t j = x % 8;
		_bit[i] |= (1 << j);
	}
	void reset(size_t x)
	{
		size_t i = x / 8;
		size_t j = x % 8;
		_bit[i] &= ~(1 << j);
	}
	bool test(size_t x)
	{
		size_t i = x / 8;
		size_t j = x % 8;
		return _bit[i] & (1 << j);
	}
private:
	vector<char> _bit;
};

template<size_t N>
class two_bit_set//设计双位图解题
{
public:
	void set(size_t x)
	{
		bool tmp1 = _bs1.test(x);
		bool tmp2 = _bs2.test(x);
		if (tmp1 == false && tmp2 == false)//00->01
		{
			_bs2.set(x);
		}
		else if (tmp1 == 0 && tmp2 == true)//01->10
		{
			_bs1.set(x);
			_bs2.reset(x);
		}
	}
	void print_once()
	{
		for (size_t i = 0; i < N; i++)
		{
			if (_bs1.test(i) == false && _bs2.test(i) == true)//01只出现一次
			{
				cout << i << endl;
			}
		}
	}
private:
	bit_set<N> _bs1;
	bit_set<N> _bs2;
};
void test1()
{
	bit_set<100> bs1;
	bs1.set(8);
	bs1.set(9);
	bs1.set(20);
	bs1.set(99);
	cout << bs1.test(8) << endl;
	cout << bs1.test(9) << endl;
	cout << bs1.test(20) << endl;
	cout << bs1.test(99) << endl;
	bs1.reset(8);
	bs1.reset(9);
	bs1.reset(20);
	bs1.reset(99);
	cout << bs1.test(8) << endl;
	cout << bs1.test(9) << endl;
	cout << bs1.test(20) << endl;
	cout << bs1.test(99) << endl;
}

void test2()
{
	bit_set<-1> bs1;
	bit_set<1024 * 1024 * 1024 * 4 - 1> bs2;
	bit_set<2 * INT_MAX - 1> bs3;
	bit_set<0xffffffff> bs4;
}