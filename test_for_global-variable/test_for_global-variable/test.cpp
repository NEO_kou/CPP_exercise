#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable:6031)

//#include<iostream>
//#include<memory>
//#include<string>
//using namespace std;
//
//
//class TEST
//{
//public:
//	TEST(int _a = 1,int _b = 2)
//		:a(_a)
//		,b(_b)
//	{}
//	int a;
//	int b;
//};
//
//int i = 10;
//

#include<iostream>
#include<thread>
#include<memory>
#include<mutex>
#include<cstdio>
using namespace std;

template<class T>
class Smart_Ptr //实现的C++11的shared_ptr版本
{
public:
	/*Smart_Ptr(const T*& ptr)
		:_ptr(ptr)
		,_pcount(new int(1))
	{ }*/

	Smart_Ptr(T* ptr = nullptr)
		:_ptr(ptr)
		, _pcount(new int(1))
		, _pmtx(new mutex)
	{}

	~Smart_Ptr()
	{
		Release();
	}

	Smart_Ptr(const Smart_Ptr<T>& sp)
		:_ptr(sp._ptr)
		, _pcount(sp._pcount)
		, _pmtx(sp._pmtx)
	{
		Addcount();
	}

	Smart_Ptr<T>& operator=(const Smart_Ptr<T>& sp)
	{
		if (_ptr != sp._ptr)
		{
			Release();
			_ptr = sp._ptr;
			_pcount = sp._pcount;
			_pmtx = sp._pmtx;
			Addcount();
		}
		return *this;
	}

	void Release()
	{
		_pmtx->lock();
		if (--(*_pcount) == 0)//销毁最后一个变量时才释放资源
		{
			delete _ptr;
			delete _pcount;
			_pmtx->unlock();
			delete _pmtx;
		}
		else _pmtx->unlock()
	}

	void Addcount()
	{
		_pmtx->lock();
		(*_pcount)++;
		_pmtx->unlock();
	}

	void Subcount()
	{
		Release();
	}

	T& operator*()
	{
		return *_ptr;
	}
	T* operator->()
	{
		return _ptr;
	}
	T* get()
	{
		return _ptr;
	}
	int use_count()
	{
		return *_pcount;
	}
private:
	T* _ptr;
	int* _pcount;
	mutex* _pmtx;
};

int main()
{
	//shared_ptr<string> sp(new string("abcd"));
	/*int i = 0;
	for (i = 0; i < 10; i++)
	{
		cout << i << " " << endl;
	}
	cout << endl;*/
	/*TEST* t = new TEST;
	cout << t->a << " " << t->b;*/
	return 0;
}