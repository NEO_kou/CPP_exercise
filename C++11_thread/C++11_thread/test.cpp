#include<iostream>
#include<vector>
#include<thread>
#include<mutex>
#include<atomic>
using namespace std;

//void func(int n,int id)
//{
//	for (int i = 0; i < n; i++)
//		cout << id << " : " << i << " " << endl;
//	cout << endl;
//}
//
//int main()
//{
//	/*thread t1(func, 10,1);
//	thread t2(func, 15,2);
//	thread t3([](int n, int num)
//		{
//			for (int i = 0; i < n; i++)
//				cout << num << " : " << i << " " << endl;
//		},20, 3);
//	t1.join();
//	t2.join();
//	t3.join();*/
//	int m = 5, n = 10;//让m个线程跑n次
//	vector<thread> vt(m);
//	for (int i = 0; i < m; i++)
//	{
//		vt[i] = thread([i, n]()
//			{
//				for (int j = 0; j < n; j++)
//					cout << i << " : " << j << " " << endl;
//				cout << endl;
//			});
//	}
//	for (int i = 0; i < m; i++)
//	{
//		vt[i].join();
//	}
//	return 0;
//}


//int x = 0;
//mutex mtx;
//
//void func(int n)//这种加锁方式要快一些
//{
//	mtx.lock();
//	for (int i = 0; i < n; i++)
//		x++;
//	mtx.unlock();
//}//并行并不一定比串行快,++操作很小,每次都加解锁会消耗大量时间
//void func(int n)
//{
//	for (int i = 0; i < n; i++)
//	{
//		mtx.lock();
//		x++;
//		mtx.unlock();
//	}
//}
//
//int main()
//{
//	int n = 10000000;
//	thread t1(func, n);
//	thread t2(func, n);
//	t1.join();
//	t2.join();
//	return 0;
//}

//atomic<int> x = 0; //使用atomic类,其中++,--,||,&&等操作都是原子的!
//void func(int n)
//{
//	for (int i = 0; i < n; i++)
//		x++;
//}
//
//int main()
//{
//	int n = 10000000;
//	thread t1(func, n);
//	thread t2(func, n);
//	t1.join();
//	t2.join();
//	return 0;
//}



//支持两个线程,一个打印奇数一个打印偶数
int main()
{
	int n = 100;
	int x = 1;
	mutex mtx;
	condition_variable cv;
	thread t1([&, n]()
		{
			while (x < n)
			{
				unique_lock<mutex> lock(mtx);
				if (x % 2 == 0)
					cv.wait(lock);
				cout << this_thread::get_id() << " : " << x << endl;
				x++;
				cv.notify_one();
			}
		});
	thread t2([&, n]()
		{
			while (x < n)
			{
				unique_lock<mutex> lock(mtx);
				if (x % 2 == 1)
					cv.wait(lock);
				cout << this_thread::get_id() << " : " << x << endl;
				x++;
				cv.notify_one();
			}
		});
	t1.join();
	t2.join();
	return 0;
}