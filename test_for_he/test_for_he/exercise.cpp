#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable:6031)

#include<iostream>
#include<math.h>
#include<vector>
using namespace std;

int main()
{
	vector<double> vv{30.74,30.74,30.75,30.75,30.74,30.75,30.73,30.73,30.73,30.72,30.74,30.76,30.77,30.73,30.73,30.78,30.73,30.73,30.74,30.68};
	double mid = 30.7385;
	double sum = 0;
	for (auto x : vv)
	{
		sum += pow(x - mid, 2);
	}
	sum /= 19;
	cout << sqrt(sum);
	return 0;
}