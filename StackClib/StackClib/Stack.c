#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable:6031)
#include"Stack.h"

void StackInit(ST* ps)
{
	assert(ps);
	ps->a = NULL;
	ps->top = 0;//top指向栈顶数据的下一位,top为-1指向栈顶数据
	ps->capacity = 0;
}

void StackPush(ST* ps, STDataType x)
{
	assert(ps);
	if (ps->top == ps->capacity)
	{
		int newcapacity = ps->capacity == 0 ? 4 : 2 * (ps->capacity);
		STDataType* tmp = (STDataType*)realloc(ps->a, sizeof(STDataType)*newcapacity);
		ps->capacity = newcapacity;
		ps->a = tmp;
	}
	ps->a[ps->top] = x;
	ps->top++;
}

void StackPrint(ST* ps)
{
	assert(ps);
	while (ps->top >0)
	{
		printf("%d ", ps->a[ps->top - 1]);
		ps->top--;
	}
	printf("\n");
}

void StackPop(ST* ps)
{
	assert(ps);
	assert(ps->top > 0);
	ps->top--;
}

STDataType StackTop(ST* ps)
{
	assert(ps);
	assert(ps->top > 0);
	return ps->a[ps->top-1];
}

int StackSize(ST* ps)
{
	assert(ps);
	return ps->top;//top等于2有a[0]和a[1]两个数据
}

bool StackEmpty(ST* ps)
{
	assert(ps);
	if (ps->top <= 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void StackDestroy(ST* ps)
{
	assert(ps);
	free(ps->a);
	ps->a = NULL;
	ps->top = 0;
	ps->capacity = 0;
}