#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<stdbool.h>


typedef int STDataType;

typedef struct Stack
{
	STDataType* a;
	int top;//栈顶
	int capacity;
}ST;


	void StackInit(ST* ps);//初始化

	void StackPrint(ST* ps);//打印

	void StackDestroy(ST* ps);//销毁

	void StackPush(ST* ps,STDataType x);//插入数据

	void StackPop(ST* ps);//删除数据

	STDataType StackTop(ST* ps);//取出栈顶数据

	int StackSize(ST* ps);//栈的大小

	bool StackEmpty(ST* ps);//判断栈是否为空
