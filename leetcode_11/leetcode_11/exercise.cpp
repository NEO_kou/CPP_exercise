class Solution {
public:
    int maxArea(vector<int>& height) {
        int n = height.size();
        int left = 0;
        int right = n - 1;
        int maxv = 0;
        while (left < right)
        {
            int minh = min(height[left], height[right]);
            maxv = max(maxv, minh * (right - left));
            if (height[left] > height[right])
                right--;
            else left++;
        }
        return maxv;
    }
};