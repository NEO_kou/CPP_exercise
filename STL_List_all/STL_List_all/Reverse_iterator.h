#pragma once
template<class Iterator, class Ref, class Ptr>
//复用正向迭代器+迭代器适配器
struct __Reverse_iterator
{
	Iterator _cur;
	typedef __Reverse_iterator<Iterator, Ref, Ptr> RIterator;

	__Reverse_iterator(Iterator it)
		:_cur(it)
	{}

	RIterator operator++()
	{
		--_cur;
		return *this;
	}

	RIterator operator++(int)
	{
		auto tmp = this;
		--_cur;
		return *tmp;
	}

	RIterator operator--()
	{
		++_cur;
		return *this;
	}

	RIterator operator--(int)
	{
		auto tmp = this;
		++_cur;
		return *tmp;
	}

	Ref operator*()//和库中的实现匹配
	{
		auto tmp = _cur;
		--tmp;
		return *tmp;
	}

	Ptr operator->()
	{
		//return _cur.operator->();
		return &(operator*());
	}

	bool operator!=(const RIterator& it)
	{
		return _cur != it._cur;
	}
};