#pragma once
#include<iostream>
#include<algorithm>
#include<assert.h>
#include"Reverse_iterator.h"
using namespace std;

template<class T>
class list_node
{
public:
	T _data;
	list_node<T>* _next;
	list_node<T>* _prev;
	list_node(const T& x = T())
		:_data(x)
		, _next(nullptr)
		, _prev(nullptr)
	{

	}
};


//迭代器是一种像指针的东西,有使用->的场景,访问结构体成员
template<class T, class Ref, class Ptr>
struct __list_iterator//迭代器不需要析函数
{
	typedef list_node<T> node;
	typedef __list_iterator iterator;

	typedef bidirectional_iterator_tag iterator_category;//为了使用find函数而定义的内容
	typedef T value_type;
	typedef Ptr pointer;
	typedef Ref reference;
	typedef ptrdiff_t difference_type;

	node* _node;
	__list_iterator(node* nnode)
		:_node(nnode)
	{

	}

	bool operator!=(const iterator& it)const
	{
		return _node != it._node;
	}

	bool operator==(const iterator& it)const
	{
		return _node == it._node;
	}

	iterator& operator++()//前置++
	{
		_node = _node->_next;
		return *this;
	}

	iterator& operator++(int)//后置++
	{
		iterator tmp = *this;
		_node = _node->_next;
		return tmp;
	}

	iterator& operator--()//前置--
	{
		_node = _node->_prev;
		return *this;
	}

	iterator& operator--(int)//后置--
	{
		iterator tmp = *this;
		_node = _node->_prev;
		return tmp;
	}

	//*it -> it.operator*() 可读可写
	/*T& operator*()
	{
		return _node->_data;
	}*/

	//只可读不可写
	//const T& operator*()//只是返回值不同不会构成函数重载
	//{
	//	return _node->_data;
	//}

	//按模板参数来
	Ref operator*()
	{
		return _node->_data;
	}

	//T* operator->()//编译器做了特殊处设立,省略了一个箭头->
	//{
	//	return &(operator*());
	//}

	//const T* operator->()const
	//{
	//	return &(operator*());
	//}
	//
	//T* operator->()
	//{
	//	//return &(operator*());
	//}

	//按模板参数来
	Ptr operator->()
	{
		return &(operator*());
	}

};



template<class T>
class List
{
	typedef list_node<T> node;
public:
	typedef __list_iterator<T, T&, T*> iterator;
	typedef __list_iterator<T, const T&, const T*> const_iterator;
	typedef __Reverse_iterator<iterator, T&, T*> reverse_iterator;  //复用普通迭代器,传普迭代器就是普通的反向迭代器
	typedef __Reverse_iterator<const_iterator, const T&, const T*> const_reverse_iterator; //传const的正向迭代器就算const的反向迭代器
	const_iterator begin()const
	{
		//iterator tmp(_head->_next);
		//return tmp;
		return const_iterator(_head->_next);
	}

	iterator begin()
	{
		//iterator tmp(_head->_next);
		//return tmp;
		return iterator(_head->_next);//使用了匿名对象
	}

	const_iterator end()const
	{
		//iterator tmp(_head);
		//return tmp;
		return const_iterator(_head);
	}

	iterator end()
	{
		//iterator tmp(_head);
		//return tmp;
		return iterator(_head);
	}

	reverse_iterator rbegin()
	{
		return reverse_iterator(end());
	}

	reverse_iterator rend()
	{
		return reverse_iterator(begin());
	}

	const_reverse_iterator rbegin()const
	{
		return const_reverse_iterator(end());
	}

	const_reverse_iterator rend()const
	{
		return const_reverse_iterator(begin());
	}

	void swap(List<T>& x)
	{
		std::swap(_head, x._head);
	}

	List()//无参构造
	{
		emptyinit();
	}

	void emptyinit()//创建并初始化哨兵位的头节点,方便给构造和拷贝构造复用
	{
		_head = new node;
		_head->_next = _head;
		_head->_prev = _head;
	}

	void clear()//清空数据,除了头节点
	{
		iterator it = begin();
		while (it != end())
		{
			it = erase(it);//erase会返回下一个节点的迭代器
		}
	}

	~List()//_head也要被删除掉
	{
		clear();
		delete _head;
		_head = nullptr;
	}

	template<class InputTterator>//有参的构造
	List(InputTterator first, InputTterator last)//可以用vector的迭代器来构造,也可以用栈和队列的迭代器来构造,设计成一个模板
	{
		emptyinit();
		while (first != last)
		{
			push_back(*first);
			first++;
		}
	}

	//lt2(lt1)
	List(const List<T>& lt)//完成深拷贝
	{
		emptyinit();
		List<T> tmp(lt.begin(), lt.end());//用lt1的迭代器区间去构造一下tmp
		::swap(_head, tmp._head);
	}

	//lt3 = lt1
	List<T>& operator=(List<T> lt)
	{
		swap(lt);
		return *this;
	}
	void push_back(const T& val)
	{
		/*node* _tail = _head->_prev;
		node* newnode = new node(val);
		_tail->_next = newnode;
		newnode -> _prev = _tail;
		newnode->_next = _head;
		_head->_prev = newnode;*/
		insert(end(), val);//在end的前面插入也就是尾插
	}

	void push_front(const T& val)
	{
		insert(begin(), val);
	}

	iterator insert(iterator pos, const T& x)
	{
		node* cur = pos._node;
		node* prev = cur->_prev;
		node* newnode = new node(x);
		newnode->_next = cur;
		cur->_prev = newnode;
		newnode->_prev = prev;
		prev->_next = newnode;
		return iterator(newnode);//匿名对象
	}

	iterator erase(iterator pos)
	{
		assert(pos != end());
		node* cur = pos._node;
		node* next = cur->_next;
		node* prev = cur->_prev;
		prev->_next = next;
		next->_prev = prev;
		delete cur;
		return iterator(next);
	}

	void pop_back()
	{
		erase(--end());
	}

	void pop_front()
	{
		erase(begin());
	}

private:
	node* _head;
};
