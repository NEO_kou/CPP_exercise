#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable:6031)
#include<iostream>
#include"Reverse_iterator.h"
#include"List.h"
#include"Vector.h"
using namespace std;
void test()
{
	List<int> lt;
	lt.push_back(1);
	lt.push_back(2);
	lt.push_back(3);
	lt.push_back(4);
	lt.push_back(5);
	List<int>::reverse_iterator rit = lt.rbegin();
	while (rit != lt.rend())
	{
		cout << *rit << " ";
		rit++;
	}
}

void test1()
{
	List<int> lt;
	lt.push_back(1);
	lt.push_back(2);
	lt.push_back(3);
	lt.push_back(4);
	lt.push_back(5);
	lt.push_back(4);
	lt.push_back(5);
	List<int>::reverse_iterator rit = lt.rbegin();
	while (rit != ++lt.rend())
	{
		cout << *rit << " ";
		rit++;
	}
}

void test2()
{
	Vector<int> vv;
	vv.reserve(10);
	vv.push_back(1);
	vv.push_back(2);
	vv.push_back(3);
	vv.push_back(4);
	vv.push_back(5);
	vv.push_back(6);
	vv.push_back(7);

	Vector<int>::reverse_iterator rit = vv.rbegin();
	while (rit != --vv.rend())
	{
		cout << *rit << " ";
		rit++;
	}
	
}

int main()
{
	test2();
	return 0;
}