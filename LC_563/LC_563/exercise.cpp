#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable:6031)
  struct TreeNode {
      int val;
      TreeNode *left;
      TreeNode *right;
      TreeNode() : val(0), left(nullptr), right(nullptr) {}
      TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
      TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
  };
 
class Solution {
public:
    int _findTilt(TreeNode* root, int& sum)
    {
        if (root == nullptr)
            return 0;
        int tmp1 = _findTilt(root->left, sum);
        int tmp2 = _findTilt(root->right, sum);
        sum += abs(tmp1 - tmp2);
        int tmp = tmp1 + tmp2;
        return root->val + tmp;
    }
    int findTilt(TreeNode* root) {
        int sum = 0;
        _findTilt(root, sum);
        return sum;
    }
};