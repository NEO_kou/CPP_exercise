////#include<iostream>
////#include<vector>
////using namespace std;
////
////void _QuickSort(vector<int>& v, int left, int right)
////{
////	if (left >= right) return;
////	int key = v[left];
////	int l = left + 1; int r = right;
////	while (l < r)
////	{
////		while (l<r && v[r]>key) r--;
////		while (l < r && v[l] < key) l++;
////		swap(v[l], v[r]);
////	}
////	swap(v[left], v[l]);
////	_QuickSort(v, left+1, l - 1);
////	_QuickSort(v, l + 1, v.size() - 1);
////}
////
////
////void QuickSort(vector<int>& v)
////{
////	_QuickSort(v, 0, v.size()-1);
////}
////
////
////int main()
////{
////	vector<int> v{ 3,2, 5, 8, 4, 7, 6, 9 };
////	QuickSort(v);
////	for (auto x : v)
////		cout << x << " ";
////	cout << endl;
////	return 0;
////}
//
//#define _CRT_SECURE_NO_WARNINGS 1
//#include <stdio.h>
//#include <stdlib.h>
//#include <time.h>
//void menu()
//{
//	printf("***************\n");
//	printf("****1.play*****\n");
//	printf("****0.exit*****\n");
//	printf("***************\n");
//
//}
//void game()
//{
//	//srand((unsigned int)time(NULL));
//	//srand(1);
//	int r = rand() % 100 + 1;
//	int guess = 0;
//	int count = 5;
//	while (count)
//	{
//
//		printf("还有%d次机会\n", count);
//		printf("请猜数\n");
//		scanf("%d", &guess);
//
//		if (guess > r)
//		{
//			printf("猜大了\n");
//		}
//		else if (guess < r)
//		{
//			printf("猜小了\n");
//
//		}
//		else
//		{
//			printf("猜中啦\n");
//			break;
//		}
//		count--;
//	}
//	if (count == 0)
//	{
//		printf("失败了\n");
//
//	}
//
//}
//int main()
//{
//	srand((unsigned int)time(NULL));
//	//srand(1);
//	int input = 0;
//	do
//
//	{
//		menu();
//		printf("请选择\n");
//		scanf("%d", &input);
//		switch (input)
//		{
//		case 1:
//			game();
//			break;
//		case 0:
//			printf("游戏结束啦\n");
//			break;
//		default:
//			printf("输入错误，请重新输入\n");
//			break;
//		}
//
//	} while (input);
//
//	return 0;
//}

//#include<iostream>
//using namespace std;
//
//int b;
//
//int main()
//{
//	int a(0);
//	cout << a << endl << b << endl;
//	return 0;
//}

#define _CRT_SECURE_NO_WARNINGS 1
#pragma once
#include<stdio.h>
#define ROW 9
#define COL 9
#define ROWS ROW+2//不用等号
#define COLS COL+2
void InitBoard(char board[ROWS][COLS], int rows, int cols, char set);//?
void display(char board[ROWS][COLS], int rows, int cols);

void InitBoard(char board[ROWS][COLS], int rows, int cols, char set)
{
	int i = 0;

	for (i = 0; i < rows; i++)
	{
		int j = 0;
		for (j = 0; j < cols; j++)
		{
			board[i][j] = set;
		}
	}

}
void display(char board[ROWS][COLS], int row, int col)
{
	int i = 0;
	//int j = 1;
	for (i = 0; i <= col; i++)
	{
		printf("%d ", i);
	}
	printf("\n");
	for (i = 1; i <= row; i++)
	{
		int j = 0;
		printf("%d ", i);
		for (j = 1; j <= col; j++)
		{
			printf("%c ", board[i][j]);
		}
		printf("\n");
	}
}

void menu()
{
	printf("**********************\n");
	printf("******* 1. play*******\n");
	printf("******* 0. exit*******\n");
	printf("**********************\n");
	printf("请选择\n");
}
void game()
{
	//定义雷的数组和展示数组
	char mine[ROWS][COLS];
	char show[ROWS][COLS];
	//初始化棋盘
	InitBoard(mine, ROWS, COLS, '0');
	InitBoard(show, ROWS, COLS, '*');
	//展示棋盘
	display(show, ROW, COL);
}

int main()
{
	int input = 1;
	while (input)
	{
		menu();
		scanf("%d", &input);
		switch (input)
		{
		case 1:
			printf("扫雷游戏\n");
			game();
			break;
		case 0:
			printf("退出游戏\n");
			break;
		default:
			printf("输入错误，请重新输入\n");
			break;
		}
	}
	return 0;
}