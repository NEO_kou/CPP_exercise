#include <iostream>
#include <vector>
using namespace std;

int main() {
    int n, m;
    scanf("%d,%d", &n, &m);
    vector<vector<int>> arr(n, vector<int>(m));
    vector<int> sumrow(n);
    vector<int> sumcol(m);
    for (int i = 0; i < n; i++)
    {
        int sum = 0;
        for (int j = 0; j < m; j++)
        {
            scanf("%d", &arr[i][j]);
            sum += arr[i][j];
        }
        sumrow[i] = sum;
    }
    for (auto x : sumrow)
        cout << x << " ";
    for (int j = 0; j < m; j++)
    {
        int sum = 0;
        for (int i = 0; i < n; i++)
            sum += arr[i][j];
        sumcol[j] = sum;
    }
    cout << endl;
    for (auto x : sumcol)
        cout << x << " ";
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < m; j++)
        {
            int tmpret = sumrow[i] + sumcol[j] - arr[i][j];
            printf("%d ", tmpret);
        }
        printf("\n");
    }
}