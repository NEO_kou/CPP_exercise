#pragma once
#include"RB_Tree.h"

template<class K>
class Set
{
	struct SetKOT
	{
		const K& operator()(const K& key)
		{
			return key;
		}
	};
public:
	typedef typename RBTree<K, K, SetKOT>::iterator iterator;
	iterator begin()
	{
		return _t.begin();
	}
	iterator end()
	{
		return _t.end();
	}
	pair<iterator, bool> insert(const K& key)
	{
		return _t.insert(key);
	}
private:
	RBTree<K, K, SetKOT> _t;
};

void testset1()
{
	Set<int> s;
	s.insert(3);
	s.insert(2);
	s.insert(1);
	s.insert(5);
	Set<int>::iterator it = s.begin();
	while (it != s.end())
	{
		cout << *it << " ";
		++it;
	}
	cout << endl;
}