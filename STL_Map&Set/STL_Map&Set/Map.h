#pragma once
#include"RB_Tree.h"

template<class K, class V>
class Map
{
	struct MapKOT
	{
		const K& operator()(const pair<K,V>& kv)
		{
			return kv.first;
		}
	};
public:
	typedef typename RBTree<K, pair<K, V>, MapKOT>::iterator iterator;
	iterator begin()
	{
		return _t.begin();
	}
	iterator end()
	{
		return _t.end();
	}
	pair<iterator,bool> insert(const pair<K, V>& kv)
	{
		return _t.insert(kv);
	}

	V& operator[](const K& key)//map的方括号是[K]返回V的引用
	{
		pair<iterator, bool> ret = insert(make_pair(key, V()));
		return ret.first->second;//ret.first是pair中的第一个参数:iterator,->second是迭代器iterator中的第二次参数V
	}
private:
	RBTree<K, pair<K, int>, MapKOT> _t;
};

void testmap1()
{
	string arr[] = { "苹果", "西瓜", "苹果", "西瓜", "苹果", "苹果", "西瓜", "苹果", "香蕉", "苹果", "香蕉" };

	Map<string, int> countMap;
	for (auto& str : arr)
	{
		// 1、str不在countMap中，插入pair(str, int()),然后在对返回次数++
		// 2、str在countMap中，返回value(次数)的引用，次数++;
		countMap[str]++;
	}

	Map<string, int>::iterator it = countMap.begin();
	while (it != countMap.end())
	{
		cout << it->first << ":" << it->second << endl;
		++it;
	}

	for (auto& kv : countMap)
	{
		cout << kv.first << ":" << kv.second << endl;
	}
}