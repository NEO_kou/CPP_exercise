#pragma once
#include<iostream>
#include<math.h>
#include<map>
#include<assert.h>
using namespace std;

enum Colour
{
	RED,
    BLACK
};
template<class T>
struct RBTreeNode  //用红黑树包装map和set(封装)
{
	RBTreeNode(const T& data)
		:_left(nullptr)
		, _right(nullptr)
		, _parent(nullptr)
		, _data(data)
		,_col(RED)//默认为红色只需和规则三杠
	{}
	RBTreeNode<T>* _left;
	RBTreeNode<T>* _right;
	RBTreeNode<T>* _parent;
	T _data; 
	Colour _col;
};

template<class T, class Ref, class Ptr>
struct __RBTree_Iterator
{
	typedef RBTreeNode<T> Node;
	typedef __RBTree_Iterator<T, Ref, Ptr> Self;
	Node* _node;

	__RBTree_Iterator(Node* node)
		:_node(node)
	{}

	Ref operator*()
	{
		return _node->_data;
	}

	Ptr operator->()
	{
		return &_node->_data;
	}

	bool operator!=(const Self& s) const
	{
		return _node != s._node;
	}

	bool operator==(const Self& s) const
	{
		return _node == s._node;
	}

	Self& operator++()
	{
		//右不为空,找到右子树中最左边的节点
		if (_node->_right)
		{
			Node* left = _node->_right;
			while (left->_left)
				left = left->_left;
			_node = left;
		}
		//右子树为空 找祖先里面孩子不是祖先右边的内个
		else
		{
			Node* parent = _node->_parent;
			Node* cur = _node;
			while (parent && cur == parent->_right)
			{
				cur = cur->_parent;
				parent = parent->_parent;
			}
			_node = parent;
		}
		return *this;
	}
	Self& operator--()//和++相反
	{
		if (_node->_left)
		{
			Node* right = _node->_left;
			while (right->_right)
				right = right->_right;
			_node = right;
		}
		else
		{
			Node* parent = _node->_parent;
			Node* cur = _node;
			while (parent && cur == parent->_left)
			{
				cur = cur->_parent;
				parent = parent->_parent;
			}
			_node = parent;
		}
		return *this;
	}
};

template<class K, class T,class KOT>
struct RBTree
{
	typedef RBTreeNode<T> Node;
	typedef __RBTree_Iterator<T, T&, T*> iterator;
public:
	iterator begin()
	{
		Node* left = _root;
		while (left && left->_left)
			left = left->_left;
		return iterator(left);
	}

	iterator end()
	{
		return iterator(nullptr);
	}

	pair<iterator,bool> insert(const T& data)//第一步:按照二叉搜索树的方式插入值,第二步:调整平衡因子后旋转
	{                         //data不知道是什么,第二个模板参数是什么data就是什么
		KOT kot;                 //为了实现运算符重载方括号,将insert的返回值改成一个pair
		if (_root == nullptr)
		{
			_root = new Node(data);
			_root->_col = BLACK;
			return make_pair(iterator(_root),true);
		}
		Node* cur = _root;
		Node* parent = nullptr;
		while (cur)
		{
			if (kot(cur->_data) < kot(data))//泛型需要仿函数来实现这一部分比较
			{
				parent = cur;
				cur = cur->_right;
			}
			else if (kot(cur->_data) > kot(data))
			{
				parent = cur;
				cur = cur->_left;
			}
			else return make_pair(iterator(cur), false);
		}
		cur = new Node(data);
		Node* ret = cur;//用一个ret保存cur,因为cur可能会旋转+变色,返回的pair应该就是最初的cur
		if (kot(parent->_data) < kot(data))
			parent->_right = cur;
		else
			parent->_left = cur;
		//此时new出来的节点的parent还指向空
		cur->_parent = parent;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////前面的过程和AVL树一致
		while (parent && parent->_col == RED)//变色＋旋转
		{
			if (parent == _root)
			{
				_root->_col = BLACK;
				break;
			}
			Node* grandf = parent->_parent;
			assert(grandf);
			assert(grandf->_col == BLACK);
			Node* uncle = nullptr;
			if (parent == grandf->_left)
				uncle = grandf->_right;
			else uncle = grandf->_left;
			if (uncle == nullptr || uncle->_col == BLACK)//uncle为空或为黑有四种情况来变色＋旋转
			{
				if (parent == grandf->_left && cur == parent->_left)//右旋＋变色
				{
					RotateR(grandf);
					parent->_col = BLACK;
					grandf->_col = RED;
					break;
				}
				else if (parent == grandf->_right && cur == parent->_right)//左旋+变色
				{
					RotateL(grandf);
					parent->_col = BLACK;
					grandf->_col = RED;
					break;
				}
				else if (parent == grandf->_left && cur == parent->_right)//先左旋再右旋再变色
				{
					RotateL(parent);
					RotateR(grandf);
					cur->_col = BLACK;
					grandf->_col = RED;
					break;
				}
				else if (parent == grandf->_right && cur == parent->_left)//先右旋再左旋再变色
				{
					RotateR(parent);
					RotateL(grandf);
					cur->_col = BLACK;
					grandf->_col = RED;
					break;
				}
			}
			else if (uncle && uncle->_col == RED)//叔叔为红,直接变色,不旋转
			{
				parent->_col = BLACK;
				uncle->_col = BLACK;
				grandf->_col = RED;
				cur = grandf;
				parent = cur->_parent;
			}
			_root->_col = BLACK;
		}
		return make_pair(iterator(ret), true);
	}
	void inorder()
	{
		_inorder(_root);
	}
	bool isbalance()
	{
		if (_root == nullptr)
			return true;
		if (_root->_col == RED)
			return false;
		int flag = 0;//黑色节点数量的基准值
		Node* cur = _root;
		while (cur)
		{
			if (cur->_col == BLACK)
				flag++;
			cur = cur->_left;
		}
		return PrevCheck(_root, 0, flag);
	}
private:
	void RotateL(Node* parent)
	{
		Node* subR = parent->_right;
		Node* subRL = subR->_left;
		parent->_right = subRL;
		if (subRL)
			subRL->_parent = parent;
		Node* ppNode = parent->_parent;
		subR->_left = parent;
		parent->_parent = subR;
		if (_root == parent)
		{
			_root = subR;
			subR->_parent = nullptr;
		}
		else
		{
			if (ppNode->_left == parent)
				ppNode->_left = subR;
			else
				ppNode->_right = subR;
			subR->_parent = ppNode;
		}
	}
	void RotateR(Node* parent)
	{
		Node* subL = parent->_left;
		Node* subLR = subL->_right;
		parent->_left = subLR;
		if (subLR)
			subLR->_parent = parent;
		Node* ppNode = parent->_parent;
		subL->_right = parent;
		parent->_parent = subL;
		if (_root == parent)
		{
			_root = subL;
			subL->_parent = nullptr;
		}
		else
		{
			if (ppNode->_left == parent)
				ppNode->_left = subL;
			else
				ppNode->_right = subL;
			subL->_parent = ppNode;
		}
	}
	void _inorder(Node* root)
	{
		if (root == nullptr)
			return;
		_inorder(root->_left);
		cout << root->_kv.first << " ";
		_inorder(root->_right);
	}
	bool PrevCheck(Node* root, int blanum,int flag)
	{
		if (root == nullptr)
		{
			if (blanum != flag)
				return false;
			else return true;
		}
		if (root->_col == BLACK)
			blanum++;
		if (root->_col == RED && root->_parent->_col == RED)
			return false;
		return PrevCheck(root->_left, blanum, flag) && PrevCheck(root->_right, blanum, flag);
	}
private:
	Node* _root = nullptr;
};

/// <summary>
/// /////////////////////////////////////////////////////////////标准版//////////////////////////////////////////////////////////////////////////////////////
/// </summary>
/// 
enum Colour
{
	RED,
	BLACK
};

template<class T>
struct RBTreeNode
{
	RBTreeNode<T>* _left;
	RBTreeNode<T>* _right;
	RBTreeNode<T>* _parent;

	T _data;
	Colour _col;

	RBTreeNode(const T& data)
		:_left(nullptr)
		, _right(nullptr)
		, _parent(nullptr)
		, _data(data)
	{}
};

template<class T, class Ref, class Ptr>
struct __RBTreeIterator
{
	typedef RBTreeNode<T> Node;
	typedef __RBTreeIterator<T, Ref, Ptr> Self;
	Node* _node;

	__RBTreeIterator(Node* node)
		:_node(node)
	{}

	Ref operator*()
	{
		return _node->_data;
	}

	Ptr operator->()
	{
		return &_node->_data;
	}

	bool operator!=(const Self& s) const
	{
		return _node != s._node;
	}

	bool operator==(const Self& s) const
	{
		return _node == s._node;
	}

	Self& operator++()
	{
		if (_node->_right)
		{
			// 下一个就是右子树的最左节点
			Node* left = _node->_right;
			while (left->_left)
			{
				left = left->_left;
			}

			_node = left;
		}
		else
		{
			// 找祖先里面孩子不是祖先的右的那个
			Node* parent = _node->_parent;
			Node* cur = _node;
			while (parent && cur == parent->_right)
			{
				cur = cur->_parent;
				parent = parent->_parent;
			}

			_node = parent;
		}

		return *this;
	}

	Self& operator--()
	{
		if (_node->_left)
		{
			// 下一个是左子树的最右节点
			Node* right = _node->_left;
			while (right->_right)
			{
				right = right->_right;
			}

			_node = right;
		}
		else
		{
			// 孩子不是父亲的左的那个祖先
			Node* parent = _node->_parent;
			Node* cur = _node;
			while (parent && cur == parent->_left)
			{
				cur = cur->_parent;
				parent = parent->_parent;
			}

			_node = parent;
		}

		return *this;
	}
};

template<class K, class T, class KeyOfT>
struct RBTree
{
	typedef RBTreeNode<T> Node;
public:
	typedef __RBTreeIterator<T, T&, T*> iterator;

	iterator begin()
	{
		Node* left = _root;
		while (left && left->_left)
		{
			left = left->_left;
		}

		return iterator(left);
	}

	iterator end()
	{
		return iterator(nullptr);
	}

	pair<iterator, bool> Insert(const T& data)
	{
		KeyOfT kot;

		if (_root == nullptr)
		{
			_root = new Node(data);
			_root->_col = BLACK;
			return make_pair(iterator(_root), true);
		}

		Node* parent = nullptr;
		Node* cur = _root;
		while (cur)
		{
			if (kot(cur->_data) < kot(data))
			{
				parent = cur;
				cur = cur->_right;
			}
			else if (kot(cur->_data) > kot(data))
			{
				parent = cur;
				cur = cur->_left;
			}
			else
			{
				return make_pair(iterator(cur), false);
			}
		}

		cur = new Node(data);
		Node* newnode = cur;
		cur->_col = RED;

		if (kot(parent->_data) < kot(data))
		{
			parent->_right = cur;
		}
		else
		{
			parent->_left = cur;
		}

		cur->_parent = parent;

		while (parent && parent->_col == RED)
		{
			Node* grandfater = parent->_parent;
			assert(grandfater);
			assert(grandfater->_col == BLACK);
			// 关键看叔叔
			if (parent == grandfater->_left)
			{
				Node* uncle = grandfater->_right;
				// 情况一 : uncle存在且为红，变色+继续往上处理
				if (uncle && uncle->_col == RED)
				{
					parent->_col = uncle->_col = BLACK;
					grandfater->_col = RED;
					// 继续往上处理
					cur = grandfater;
					parent = cur->_parent;
				}// 情况二+三：uncle不存在 + 存在且为黑
				else
				{
					// 情况二：右单旋+变色
					//     g 
					//   p   u
					// c
					if (cur == parent->_left)
					{
						RotateR(grandfater);
						parent->_col = BLACK;
						grandfater->_col = RED;
					}
					else
					{
						// 情况三：左右单旋+变色
						//     g 
						//   p   u
						//     c
						RotateL(parent);
						RotateR(grandfater);
						cur->_col = BLACK;
						grandfater->_col = RED;
					}

					break;
				}
			}
			else // (parent == grandfater->_right)
			{
				Node* uncle = grandfater->_left;
				// 情况一
				if (uncle && uncle->_col == RED)
				{
					parent->_col = uncle->_col = BLACK;
					grandfater->_col = RED;
					// 继续往上处理
					cur = grandfater;
					parent = cur->_parent;
				}
				else
				{
					// 情况二：左单旋+变色
					//     g 
					//   u   p
					//         c
					if (cur == parent->_right)
					{
						RotateL(grandfater);
						parent->_col = BLACK;
						grandfater->_col = RED;
					}
					else
					{
						// 情况三：右左单旋+变色
						//     g 
						//   u   p
						//     c
						RotateR(parent);
						RotateL(grandfater);
						cur->_col = BLACK;
						grandfater->_col = RED;
					}

					break;
				}
			}

		}

		_root->_col = BLACK;
		return make_pair(iterator(newnode), true);
	}

	void InOrder()
	{
		_InOrder(_root);
		cout << endl;
	}

	bool IsBalance()
	{
		if (_root == nullptr)
		{
			return true;
		}

		if (_root->_col == RED)
		{
			cout << "根节点不是黑色" << endl;
			return false;
		}

		// 黑色节点数量基准值
		int benchmark = 0;
		/*Node* cur = _root;
		while (cur)
		{
		if (cur->_col == BLACK)
		++benchmark;

		cur = cur->_left;
		}*/

		return PrevCheck(_root, 0, benchmark);
	}

private:
	bool PrevCheck(Node* root, int blackNum, int& benchmark)
	{
		if (root == nullptr)
		{
			//cout << blackNum << endl;
			//return;
			if (benchmark == 0)
			{
				benchmark = blackNum;
				return true;
			}

			if (blackNum != benchmark)
			{
				cout << "某条黑色节点的数量不相等" << endl;
				return false;
			}
			else
			{
				return true;
			}
		}

		if (root->_col == BLACK)
		{
			++blackNum;
		}

		if (root->_col == RED && root->_parent->_col == RED)
		{
			cout << "存在连续的红色节点" << endl;
			return false;
		}

		return PrevCheck(root->_left, blackNum, benchmark)
			&& PrevCheck(root->_right, blackNum, benchmark);
	}

	void _InOrder(Node* root)
	{
		if (root == nullptr)
		{
			return;
		}

		_InOrder(root->_left);
		cout << root->_kv.first << ":" << root->_kv.second << endl;
		_InOrder(root->_right);
	}

	void RotateL(Node* parent)
	{
		Node* subR = parent->_right;
		Node* subRL = subR->_left;

		parent->_right = subRL;
		if (subRL)
			subRL->_parent = parent;

		Node* ppNode = parent->_parent;

		subR->_left = parent;
		parent->_parent = subR;

		if (_root == parent)
		{
			_root = subR;
			subR->_parent = nullptr;
		}
		else
		{
			if (ppNode->_left == parent)
			{
				ppNode->_left = subR;
			}
			else
			{
				ppNode->_right = subR;
			}

			subR->_parent = ppNode;
		}

	}

	void RotateR(Node* parent)
	{
		Node* subL = parent->_left;
		Node* subLR = subL->_right;

		parent->_left = subLR;
		if (subLR)
		{
			subLR->_parent = parent;
		}

		Node* ppNode = parent->_parent;

		subL->_right = parent;
		parent->_parent = subL;

		if (_root == parent)
		{
			_root = subL;
			subL->_parent = nullptr;
		}
		else
		{
			if (ppNode->_left == parent)
			{
				ppNode->_left = subL;
			}
			else
			{
				ppNode->_right = subL;
			}

			subL->_parent = ppNode;
		}

	}

private:
	Node* _root = nullptr;
};

//void TestRBTree1()
//{
//	int a[] = { 4, 2, 6, 1, 3, 5, 15, 7, 16, 14, 0,5,30,25,20,4,13,30,28,27};  // 测试双旋平衡因子调节
//	//int a[] = { 16, 3, 7, 11, 9, 26, 18, 14, 15 };
//	RBTree<int, int> t1;
//	for (auto e : a)
//	{
//		t1.Insert(make_pair(e, e));
//	}
//
//	t1.InOrder();
//	cout << "IsBalance:" << t1.IsBalance() << endl;
//}
//
//void TestRBTree2()
//{
//	size_t N = 1000;
//	srand(time(0));
//	RBTree<int, int> t1;
//	for (size_t i = 0; i < N; ++i)
//	{
//		int x = rand();
//		cout << "Insert:" << x << ":" << i << endl;
//		t1.Insert(make_pair(x, i));
//	}
//	cout << "IsBalance:" << t1.IsBalance() << endl;
//}
//void test1()
//{
//	int a[] = { 16, 3, 7, 11, 9, 26, 18, 14, 15 };
//	//int a[] = { 4, 2, 6, 1, 3, 5, 15, 7, 16, 14 };
//	RBTree<int, int> rb1;
//	for (auto x : a)
//		rb1.insert(make_pair(x, x));
//	rb1.inorder();
//	rb1.isbalance();
//}
//
//void test2()
//{
//	RBTree<int, int> t1;
//	size_t N = 10000000;
//	srand(time(0));
//	for (int i = 0; i < N; i++)
//	{
//		int x = rand();
//		t1.insert(make_pair(x, i));
//	}
//	cout << "是否平衡: " << t1.isbalance() << endl;
//}