class Solution {
public:
    int lengthOfLastWord(string s) {
        int ret = 0;
        int n = s.size();
        int index = n - 1;
        for (index = n - 1; index >= 0; index--)
        {
            if (s[index] != ' ')
                break;
        }
        for (int i = index; i >= 0; i--)
        {
            if (s[i] == ' ')
            {
                ret = index - i;
                break;
            }
        }
        if (ret != 0)
            return ret;
        else return index + 1;
    }
};