#pragma once
#include<iostream>
#include<math.h>
#include<map>
#include<assert.h>
using namespace std;

enum Colour
{
	RED,
    BLACK
};
template<class K,class V>
struct RBTreeNode
{
	RBTreeNode(const pair<K, V>& kv)
		:_left(nullptr)
		, _right(nullptr)
		, _parent(nullptr)
		, _kv(kv)
		,_col(RED)//默认为红色只需和规则三杠
	{}
	RBTreeNode<K, V>* _left;
	RBTreeNode<K, V>* _right;
	RBTreeNode<K, V>* _parent;
	pair<K, V> _kv; 
	Colour _col;
};

template<class K, class V>
struct RBTree
{
	typedef RBTreeNode<K, V> Node;
public:
	bool insert(const pair<K, V>& kv)//第一步:按照二叉搜索树的方式插入值,第二步:调整平衡因子后旋转
	{
		if (_root == nullptr)
		{
			_root = new Node(kv);
			_root->_col = BLACK;
			return true;
		}
		Node* cur = _root;
		Node* parent = nullptr;
		while (cur)
		{
			if (cur->_kv.first < kv.first)
			{
				parent = cur;
				cur = cur->_right;
			}
			else if (cur->_kv.first > kv.first)
			{
				parent = cur;
				cur = cur->_left;
			}
			else return false;
		}
		cur = new Node(kv);
		if (parent->_kv.first < kv.first)
			parent->_right = cur;
		else
			parent->_left = cur;
		//此时new出来的节点的parent还指向空
		cur->_parent = parent;
///////////////////////////////////////////////////////////////////////////////////前面的过程和AVL树一致
		while (parent && parent->_col == RED)//变色＋旋转
		{
			if (parent == _root)
			{
				_root->_col = BLACK;
				break;
			}
			Node* grandf = parent->_parent;
			assert(grandf);
			assert(grandf->_col == BLACK);
			Node* uncle = nullptr;
			if (parent == grandf->_left)
				uncle = grandf->_right;
			else uncle = grandf->_left;
			if (uncle == nullptr || uncle->_col == BLACK)//uncle为空或为黑有四种情况来变色＋旋转
			{
				if (parent == grandf->_left && cur == parent->_left)//右旋＋变色
				{
					RotateR(grandf);
					parent->_col = BLACK;
					grandf->_col = RED;
					break;
				}
				else if (parent == grandf->_right && cur == parent->_right)//左旋+变色
				{
					RotateL(grandf);
					parent->_col = BLACK;
					grandf->_col = RED;
					break;
				}
				else if (parent == grandf->_left && cur == parent->_right)//先左旋再右旋再变色
				{
					RotateL(parent);
					RotateR(grandf);
					cur->_col = BLACK;
					grandf->_col = RED;
					break;
				}
				else if (parent == grandf->_right && cur == parent->_left)//先右旋再左旋再变色
				{
					RotateR(parent);
					RotateL(grandf);
					cur->_col = BLACK;
					grandf->_col = RED;
					break;
				}
			}
			else if (uncle && uncle->_col == RED)//叔叔为红,直接变色,不旋转
			{
				parent->_col = BLACK;
				uncle->_col = BLACK;
				grandf->_col = RED;
				cur = grandf;
				parent = cur->_parent;
			}
			_root->_col = BLACK;
		}
		return true;
	}
	void inorder()
	{
		_inorder(_root);
	}
	bool isbalance()
	{
		if (_root == nullptr)
			return true;
		if (_root->_col == RED)
			return false;
		int flag = 0;//黑色节点数量的基准值
		Node* cur = _root;
		while (cur)
		{
			if (cur->_col == BLACK)
				flag++;
			cur = cur->_left;
		}
		return PrevCheck(_root, 0, flag);
	}
private:
	void RotateL(Node* parent)
	{
		Node* subR = parent->_right;
		Node* subRL = subR->_left;
		parent->_right = subRL;
		if (subRL)
			subRL->_parent = parent;
		Node* ppNode = parent->_parent;
		subR->_left = parent;
		parent->_parent = subR;
		if (_root == parent)
		{
			_root = subR;
			subR->_parent = nullptr;
		}
		else
		{
			if (ppNode->_left == parent)
				ppNode->_left = subR;
			else
				ppNode->_right = subR;
			subR->_parent = ppNode;
		}
	}
	void RotateR(Node* parent)
	{
		Node* subL = parent->_left;
		Node* subLR = subL->_right;
		parent->_left = subLR;
		if (subLR)
			subLR->_parent = parent;
		Node* ppNode = parent->_parent;
		subL->_right = parent;
		parent->_parent = subL;
		if (_root == parent)
		{
			_root = subL;
			subL->_parent = nullptr;
		}
		else
		{
			if (ppNode->_left == parent)
				ppNode->_left = subL;
			else
				ppNode->_right = subL;
			subL->_parent = ppNode;
		}
	}
	void _inorder(Node* root)
	{
		if (root == nullptr)
			return;
		_inorder(root->_left);
		cout << root->_kv.first << " ";
		_inorder(root->_right);
	}
	bool PrevCheck(Node* root, int blanum,int flag)
	{
		if (root == nullptr)
		{
			if (blanum != flag)
				return false;
			else return true;
		}
		if (root->_col == BLACK)
			blanum++;
		if (root->_col == RED && root->_parent->_col == RED)
			return false;
		return PrevCheck(root->_left, blanum, flag) && PrevCheck(root->_right, blanum, flag);
	}
private:
	Node* _root = nullptr;
};

void test1()
{
	int a[] = { 16, 3, 7, 11, 9, 26, 18, 14, 15 };
	//int a[] = { 4, 2, 6, 1, 3, 5, 15, 7, 16, 14 };
	RBTree<int, int> rb1;
	for (auto x : a)
		rb1.insert(make_pair(x, x));
	rb1.inorder();
	rb1.isbalance();
}

void test2()
{
	RBTree<int, int> t1;
	size_t N = 10000000;
	srand(time(0));
	for (int i = 0; i < N; i++)
	{
		int x = rand();
		t1.insert(make_pair(x, i));
	}
	cout << "是否平衡: " << t1.isbalance() << endl;
}