#include <iostream>
#include <vector>
#include <pthread.h>
#include <vector>
#include <queue>
#include <mutex>
#include <unistd.h>
#include <functional>
using namespace std;

typedef function<void(int)> ffunt_t;

void test(int x)
{
    cout << x << endl;
}

class Task
{
public:
    Task(ffunt_t func = test) : _func(func)
    {
    }
    ffunt_t _func;
};

class ThreadPool
{
public:
    void Push(Task t)
    {
        pthread_mutex_lock(&_mtx);
        _q.push(t);
        pthread_mutex_unlock(&_mtx);
        pthread_cond_signal(&_cond);
    }
    void Pop(Task &t)
    {
        
        t = _q.front();
        _q.pop();
    }

    static ThreadPool *GetInstance(int num = 10)
    {
        if (_single == nullptr)
        {
            _sigmtx.lock();
            if (_single == nullptr)
            {
                _single = new ThreadPool(num);
                _single->InitThreadPool();
            }
            _sigmtx.unlock();
        }
        return _single;
    }

private:
    ThreadPool(int num = 10) : _num(num)
    {
        pthread_mutex_init(&_mtx, nullptr);
        pthread_cond_init(&_cond, nullptr);
        _p.resize(_num);
    }
    ThreadPool(const ThreadPool &tp) = delete;

    void InitThreadPool()
    {
        for (int i = 0; i < _num; i++)
        {
            pthread_t tid
            if (pthread_create(&tid, nullptr, Routine, this) != 0)
                cout << "线程启动成功" << endl;
        }
    }
    static void *Routine(void *args)
    {
        ThreadPool *tp = (ThreadPool *)args;
        while (1)
        {
            pthread_mutex_lock(&(tp->_mtx));
            while (tp->_q.empty())
            {
                cout << "线程等待中" << endl;
                pthread_cond_wait(&(tp->_cond), &(tp->_mtx));
            }
            cout << "开始执行任务" << endl;
            Task t;
            tp->Pop(t);
            pthread_mutex_unlock(&(tp->_mtx));
            t._func(10);
        }
    }
private:
    pthread_cond_t _cond;
    pthread_mutex_t _mtx;
    queue<Task> _q; // 任务队列
    static ThreadPool *_single;
    static mutex _sigmtx;
public:
    int _num; // 线程数量
};
ThreadPool *ThreadPool::_single = nullptr;
mutex ThreadPool::_sigmtx;

int main()
{
    ThreadPool *tp = ThreadPool::GetInstance();
    while (1)
    {
        tp->Push(Task());
        sleep(1);
    }
}