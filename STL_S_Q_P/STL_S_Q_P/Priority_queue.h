#pragma once
#include<vector>
#include<iostream>
#include<algorithm>
using namespace std;

//大堆
template<class T, class Container = vector<T>>
class Priority_queue
{
public:

	template<class InputIterator>
	Priority_queue(InputIterator first, InputIterator last)//用迭代器区间构造
	{
		while (first != last)//先全部push进去
		{
			_con.push_back(*first);
			first++;
		}
		//再用此数组建堆
		for (int i = (_con.size() - 1 - 1) / 2; i >= 0; i--)
		{
			adjust_down(i);
		}
	}

	Priority_queue()//在初始化阶段会在初始化列表调用它的默认构造
	{

	}
	void adjust_up(size_t child)
	{
		size_t parent = (child - 1) / 2;
		while (child > 0)
		{
			if (_con[child] >= _con(parent))
			{
				std::swap(_con[parent], _con[child]);
				child = parent;
				parent = (child - 1) / 2;
			}
			else
			{
				break;
			}
		}
	}

	void adjust_down(size_t parent)
	{
		size_t child = parent * 2 + 1;
		while (child < _con.size())
		{
			if (_con[child] < _con[child + 1] && child + < _con.size)
			{
				child += 1;
			}
			if (_con[parent] <= _con[child])
			{
				::swap(_con[parent], _con[child]);
				parent = child;
				child = parent * 2 + 1;
			}
			else
			{
				break;
			}
		}
	}

	void push(const T& val)//堆的插入
	{
		_con.push_back(val);
		adjust_up(_con.size() - 1);
	}

	void pop()//堆的删除
	{
		std::swap(_con[0], _con[_con.size() - 1]);
		_con.pop_back();
		adjust_down(0);
	}

	const T& top()
	{
		return _con[0];
	}

	bool empty()
	{
		return _con.empty();
	}

	size_t size()
	{
		return _con.size();
	}
private:
	Container _con;

};