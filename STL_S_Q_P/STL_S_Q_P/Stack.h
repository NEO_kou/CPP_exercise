#pragma once

#include<iostream>
#include<vector>
#include<deque>
using namespace std;


template<class T, class Container = deque<T>>//模板只有T的话,底层实现就只能使用vector,就订死了,没有体现出适配器的作用
class Stack
{
public:
	void push(const T& val)
	{
		_con.push_back(val);
	}

	void pop()
	{
		_con.pop_back();
	}

	T& top()//可读可写
	{
		return _con.back();
	}

	const T& top() const
	{
		return _con.back();
	}

	bool empty() const
	{
		return _con.empty();
	}

	size_t size() const
	{
		return _con.size();
	}

private:
	//vector<T> _con;
	Container _con;
};