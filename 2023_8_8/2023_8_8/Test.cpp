#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable:6031)

#include<iostream>
using namespace std;

class A
{
protected:
	void print()
	{
		cout << "you can see me?";
	}
	A(int a = 0)
		:_a(a)
	{}
private:

	int _a;
};

class B : public A
{
public:
	void Print()
	{
		print();
		cout << _a;
	}

	B( int b = 0)
		:_b(b)
	{}

private:
	int _b;

};

int main()
{
	B b;
	b.Print();
	return 0;
}