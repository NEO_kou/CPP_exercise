#include<iostream>
#include<vector>
using namespace std;

int jump(vector<int>& nums) {
    int n = nums.size();
    vector<int> dp(n, 0x3f3f3f);
    dp[n - 1] = 0;
    for (int i = n - 2; i >= 0; i++)
    {
        if (nums[i] >= n - i - 1)
            dp[i] = 1;
        else
        {
            int minvalue = 0x3f3f3f;
            for (int j = i; j <= i + nums[i] && j < n; j++)
            {
                minvalue = min(minvalue, dp[j]);
            }
            dp[i] = minvalue + 1;
        }
    }
    return dp[0];
}
int main()
{
    vector<int> v{ 2,3,1,1,4 };
    cout << jump(v) << endl;
}