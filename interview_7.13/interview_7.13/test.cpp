//最长回文子串
//对于长度为n的一个字符串A（仅包含数字，大小写英文字母），请设计一个高效算法，计算其中最长回文子串的长度。
#include<iostream>
#include<stdio.h>
#include<cstring>
#include<string>
using namespace std;

int main()
{
	string str;
	getline(cin,str);
	int ret = -0x3f3f3f;
	for (int i = 0; i < str.size()-1; i++)
	{
		int right = str.size() - 1;
		int left = i;
		while (true)
		{
			if (right <= left) break;
			while (right > left)
			{
				if (str[right] == str[left])
					break;
				right--;
			}
			int tmp = right;
			while (i < right && str[left] == str[right])
			{
				left++;
				right--;
			}
			if (left == right || left = right + 1)
			{
				if (str[left] == str[right]) ret = max(ret, abs(i - left) * 2 + 1);
				else ret = max(ret, abs(i - left) * 2);
				break;
			}
			else right = tmp - 1;
		}
	}
	return 0;
}