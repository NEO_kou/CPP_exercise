#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable:6031)

#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable:6031)
#include<iostream>
#include<stack>
#include<queue>
#include<list>
#include<functional>
#include"Stack.h"
using namespace std;

//void testS()
//{
//	stack<int> st;
//	st.push(1);
//	st.push(2);
//	st.push(3);
//	st.push(4);
//
//	while (!st.empty())
//	{
//		cout << st.top() << " ";
//		st.pop();
//	}
//}
//
//void testQ()
//{
//	queue<int> q;
//	q.push(1);
//	q.push(2);
//	q.push(3);
//	q.push(4);
//
//	while (!q.empty())
//	{
//		cout << q.front() << " ";
//		q.pop();
//	}
//}
//
//void test1()
//{
//	//Stack<int,vector<int>> st;//这里需要传两个模板参数
//	Stack<int, list<int>> st;//vector可以尾插尾删,list也可以做到,底层就可以使用list实现
//	st.push(1);
//	st.push(2);
//	st.push(3);
//	st.push(4);
//	st.push(5);
//
//	st.pop();
//	while (!st.empty())
//	{
//		cout << st.top() << " ";
//		st.pop();
//	}
//}
//
//void test2()
//{
//	//Queue<int,vector<int>> q; //vector 没有支持头删的接口(pop_front)
//	// 
//	//Queue<int, list<int>> q;  //list可以实现
//	Queue<int> q; //默认的适配器
//	q.push(1);
//	q.push(2);
//	q.push(3);
//	q.push(4);
//	q.push(5);
//
//	while (!q.empty())
//	{
//		cout << q.front() << " ";
//		q.pop();
//	}
//}

//void test3()
//{
//	/*priority_queue<int> pq;
//	pq.push(1);
//	pq.push(2);
//	pq.push(9);
//	pq.push(3);
//	pq.push(1);
//	pq.push(4);
//
//	while (!pq.empty())
//	{
//		cout << pq.top() << " ";
//		pq.pop();
//	}*/
//
//	int a[] = { 3,2,7,9,4,6,1,5,8 };
//
//	priority_queue<int,vector<int>,greater<int>> pq(a, a + 9);//迭代器区间
//
//	while (!pq.empty())
//	{
//		cout << pq.top() << " ";
//		pq.pop();
//	}
//}

//void test4()
//{
//	Priority_queue<int, vector<int>> pq;
//	pq.push(5);
//	pq.push(7);
//	pq.push(3);
//	pq.push(4);
//	pq.push(10);
//	pq.push(2);
//	pq.push(1);
//
//	while (!pq.empty())
//	{
//		cout << pq.top() << " ";
//		pq.pop();
//	}
//}
//

int main()
{
	test4();
	return 0;
}