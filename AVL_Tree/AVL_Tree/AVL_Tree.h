﻿#pragma once
#include<iostream>
#include<map>
#include<algorithm>
#include<assert.h>
#include<time.h>
using namespace std;

template<class K, class V>
struct AVLTreeNode   //高度平衡二叉搜索树
{
	AVLTreeNode(const pair<K,V>& kv)
		:_left(nullptr)
		,_right(nullptr)
		,_parent(nullptr)
		,_kv(kv)
		,_bf(0)
	{}
	//用三叉链,方便更新祖先的平衡因子
	AVLTreeNode<K, V>* _left;
	AVLTreeNode<K, V>* _right;
	AVLTreeNode<K, V>* _parent;
	pair<K, V> _kv; //存储的数据
	int _bf; //balance factor平衡因子
};

template<class K,class V>
struct AVLTree
{
	typedef AVLTreeNode<K, V> Node;
public:
	bool insert(const pair<K, V>& kv)//第一步:按照二叉搜索树的方式插入值,第二步:调整平衡因子后旋转
	{
		if (_root == nullptr)
		{
			_root = new Node(kv);
			return true;
		}
		Node* cur = _root;
		Node* parent = nullptr;
		while (cur)
		{
			if (cur->_kv.first < kv.first)
			{
				parent = cur;
				cur = cur->_right;
			}
			else if (cur->_kv.first > kv.first)
			{
				parent = cur;
				cur = cur->_left;
			}
			else return false;
		}
		cur = new Node(kv);
		if (parent->_kv.first < kv.first)
			parent->_right = cur;
		else
			parent->_left = cur;
		//此时new出来的节点的parent还指向空
		cur->_parent = parent;

		//插入完成后,此时需要查看平衡因子来控制平衡
		//沿着插入的位置往上更新平衡因子 
		while (parent)
		{
			if (cur == parent->_right)
				parent->_bf++;
			else
				parent->_bf--;
			if (parent->_bf == 0)
				break;
			else if (parent->_bf == 1 || parent->_bf == -1)//若高度出现变化,需要往上更新
			{
				parent = parent->_parent;
				cur = cur->_parent;
			}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			else if (abs(parent->_bf) == 2)//说明parent所在的子树不平衡了,需要旋转处理
			{
				if (parent->_bf == 2 && cur->_bf == 1) //这种情况为新节点插入较高右子树的右侧,左旋转
					RotateL(parent);
				else if (parent->_bf == -2 && cur->_bf == -1) //这种情况为新节点插入较高左子树的左侧,右旋转
					RotateR(parent);
				else if (parent->_bf == 2 && cur->_bf == -1) //这种情况为新节点插入较高右子树的左侧,先右旋再左旋
					RotateRL(parent);
				else if (parent->_bf == -2 && cur->_bf == 1) //这种情况为新节点插入较高左子树的右侧,先左旋再右旋
					RotateLR(parent);
				break;//每插入一次只会调整一次,调整完后就break
			}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			else assert(false);//此时说明这棵树之前就有问题!
				
		}
		return true;
	}
	void inorder()//中序遍历:遍历完也就是有序的
	{
		_inorder(_root);
	}
	int height()
	{
		return _height(_root);
	}
	bool isbalance()
	{
		return _isbalance(_root);
	}
private:
	void RotateL(Node* parent)
	{
		Node* subR = parent->_right;
		Node* subRL = subR->_left;
		parent->_right = subRL;
		if (subRL)
			subRL->_parent = parent;
		Node* ppNode = parent->_parent;
		subR->_left = parent;
		parent->_parent = subR;
		if (_root == parent)
		{
			_root = subR;
			subR->_parent = nullptr;
		}
		else
		{
			if (ppNode->_left == parent)
				ppNode->_left = subR;
			else
				ppNode->_right = subR;
			subR->_parent = ppNode;
		}

		subR->_bf = parent->_bf = 0;
	}
	void RotateR(Node* parent)
	{
		Node* subL = parent->_left;
		Node* subLR = subL->_right;
		parent->_left = subLR;
		if (subLR)
			subLR->_parent = parent;
		Node* ppNode = parent->_parent;
		subL->_right = parent;
		parent->_parent = subL;
		if (_root == parent)
		{
			_root = subL;
			subL->_parent = nullptr;
		}
		else
		{
			if (ppNode->_left == parent)
				ppNode->_left = subL;
			else
				ppNode->_right = subL;
			subL->_parent = ppNode;
		}
		subL->_bf = parent->_bf = 0;
	}
	void RotateLR(Node* parent)
	{
		Node* subL = parent->_left;
		Node* subLR = subL->_right;
		int bf = subLR->_bf;
		RotateL(parent->_left);
		RotateR(parent);
		subLR->_bf = 0;
		if (bf == 1)
		{
			parent->_bf = 0;
			subL->_bf = -1;
		}
		else if (bf == -1)
		{
			parent->_bf = 1;
			subL->_bf = 0;
		}
		else if (bf == 0)
		{
			parent->_bf = 0;
			subL->_bf = 0;
		}
		else assert(false);
	}
	void RotateRL(Node* parent)
	{
		Node* subR = parent->_right;
		Node* subRL = subR->_left;
		int bf = subRL->_bf;
		RotateR(parent->_right);
		RotateL(parent);
		subRL->_bf = 0;
		if (bf == 1)
		{
			subR->_bf = 0;
			parent->_bf = -1;
		}
		else if (bf == -1)
		{
			subR->_bf = 1;
			parent->_bf = 0;
		}
		else if (bf == 0)
		{
			parent->_bf = 0;
			subR->_bf = 0;
		}
		else assert(false);
	}
	void _inorder(Node* root)
	{
		if (root == nullptr)
			return;
		_inorder(root->_left);
		cout << root->_kv.first << " ";
		_inorder(root->_right);
	}
	int _height(Node* root)
	{
		if (root == nullptr)
			return 0;
		int leftheight = _height(root->_left);
		int rightheight = _height(root->_right);
		return max(leftheight, rightheight) + 1;
	}
	bool _isbalance(Node* root)//检验二叉树是否平衡
	{
		if (root == nullptr)
			return true;
		int lefth = _height(root->_left);
		int righth = _height(root->_right);
		int gap = righth - lefth;
		if (gap != root->_bf)
		{
			cout << root->_kv.first << " " << "平衡因子异常";
			return false;
		}
		return abs(gap) < 2 && _isbalance(root->_left) && _isbalance(root->_right);
	}
private:
	Node* _root = nullptr;
};

void test1()
{
	//int a[] = { 16, 3, 7, 11, 9, 26, 18, 14, 15 };
	int a[] = { 4, 2, 6, 1, 3, 5, 15, 7, 16, 14 };
	AVLTree<int, int> t1;
	for (auto x : a)
		t1.insert(make_pair(x,x));
	t1.inorder();
	cout << t1.isbalance();
}
void test2()
{
	AVLTree<int, int> t1;
	size_t N = 1000000;
	srand(time(0));
	for (int i = 0; i < N; i++)
	{
		int x = rand();
		t1.insert(make_pair(x, i));
	}
	cout << "是否平衡: " << t1.isbalance() << endl;	
}