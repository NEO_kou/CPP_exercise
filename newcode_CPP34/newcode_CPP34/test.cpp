//#include <iostream>
//#include <vector>
//using namespace std;
//
//vector<vector<int>> ret;
//vector<int> path;
//vector<bool> check;
//int sum = 0;
//void dfs(int index, int n, int m) {
//    if (sum == m) {
//        ret.push_back(path);
//        return;
//    }
//    if (sum > m) return;
//    for (int i = index; i <= n; i++) {
//        if (check[i] == false && sum + i <= m) {
//            check[i] = true;
//            sum += i;
//            path.push_back(i);
//            dfs(index + 1, n, m);
//            check[i] = false;
//            path.pop_back();
//            sum -= i;
//        }
//    }
//}
//
//int main() {
//    int n, m;
//    cin >> n >> m;
//    check.resize(n + 1, false);
//    dfs(1, n, m);
//    for (auto& x : ret) {
//        for (auto& y : x)
//            cout << y << " ";
//        cout << endl;
//    }
//}


#include<iostream>
#include<vector>
using namespace std;
int main()
{
	vector<int> v{ 1, 2, 3, 4, 5, 6 ,7,8,9,10,11,12,13,14,15,16,17,18,19,20};
	auto pos = v.begin();
	pos = v.erase(pos);
	cout << *pos << endl;
	return 0;
}