#include<stdio.h>
#include<iostream>
using namespace std;

int dfs(int n)
{
	if (n == 1) return n;
	int sum = n;
	sum *= dfs(n - 1);
	return sum;
}

int main()
{
	int sum = 0;
	for (int i = 1; i <= 5; i++)
		sum += dfs(i);
	cout << sum << endl;
	return 0;
}