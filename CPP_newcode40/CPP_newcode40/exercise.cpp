#include <iostream>
#include <vector>
using namespace std;

int main() {
    int n; cin >> n;
    vector<int> v1(n);
    vector<int> v2(n);
    for (int i = 0; i < n; i++)
    {
        int x; cin >> x;
        v1[i] = x;
        v2[n - i - 1] = x;
    }
    int ret = 0;
    vector<int> dp1(n, 1);
    vector<int> dp2(n, 1);
    for (int i = 1; i < n; i++)
    {
        for (int j = 0; j < i; j++)
        {
            if (v1[j] < v1[i])
                dp1[i] = max(dp1[i], dp1[j] + 1);
            if (v2[j] < v2[i])
                dp2[i] = max(dp2[i], dp2[j] + 1);
        }
    }
    for (int i = 0; i < n; i++)
    {
        int tmp = dp1[i] + dp2[n - i - 1];
        ret = max(ret, tmp);
    }
    cout << n - ret + 1;
}