#define _CRT_SECURE_NO_WARNINGS 1
#pragma warning(disable:6031)
#include"Date.h"


void Date::Print()const
{
	cout << _year << " " << _month << " " << _day << endl;
}

void Date::Print()
{
	cout << _year << " " << _month << " " << _day << endl;
}

bool Date::operator==(const Date& d)const
{
	return d._year == _year
		&& d._month == _month
		&& d._day == _day;
}

//bool Date::operator!=(const Date& d)
//{
//	return d._year != _year
//		|| d._month != _month
//		|| d._day != _day;
//}

bool Date::operator!=(const Date& d)const
{
	return !(*this == d);
}

//d1>d3(d)
bool Date::operator>(const Date& d)const
{
	if (_year > d._year)
	{
		return true;
	}
	else if (_year==d._year&&_month > d._month)
	{
		return true;
	}
	else if (_year==d._year&&_month==d._month&&_day > d._day)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool Date::operator>=(const Date& d)const
{
	return (*this > d) || (*this == d);
}

bool Date::operator<(const Date& d)const
{
	return !(*this >= d);
}

bool Date::operator<=(const Date& d)const
{
	return !(*this > d);
}

Date& Date::operator+=(int day)//这里实现的功能是+=,因为d1本身的值改变了.并且这里用传引用返回?
{
	if (day < 0)//当操作符为负数时复用减法
	{
		return *this -= -day;
	}

	_day += day;
	while (_day > GetMonthDay(_year, _month))
	{
		_day -= GetMonthDay(_year, _month);
		_month++;
		if (_month == 13)
		{
			_month = 1;
			_year++;
		}
	}
	return *this;//this为指向当前对象的指针,*this就是日期类本身
}

//Date Date::operator+(int day)//这个才是不会改变当前日期类d1的函数
//{
//	Date dd(*this);//拷贝构造的场景
//	dd._day += day;
//	while (dd._day > GetMonthDay(dd._year, dd._month))
//	{
//		dd._day -= GetMonthDay(dd._year, dd._month);
//		dd._month++;
//		if (dd._month == 13)
//		{
//			dd._month = 1;
//			dd._year++;
//		}
//	}
//	return dd;
//}

Date Date::operator+(int day)const //出了作用域ret就销毁了,所以不能用引用返回
{
	Date ret = *this;
	ret += day;
	return ret;
}

Date& Date::operator++()//前置++
{
	*this += 1;
	return *this;
	//return *this +=1;
}

Date Date::operator++(int)//后置++,只能用值传递返回
{
	Date tmp(*this);
	*this += 1;
	return tmp;
}

Date& Date::operator-=(int day)
{
	if (day < 0)
	{
		return *this += -day;
	}

	_day -= day;
	while (_day <= 0)
	{
		--_month;
		if (_month == 0)
		{
			_month = 12;
			_year--;
		}
		_day += GetMonthDay(_year, _month);
	}

	return *this;
}

//Date Date::operator-(int day)// 日期-天数=日期
//{
//	Date d(*this);
//	d._day -= day;
//	while (d._day <= 0)
//	{
//		int days = GetMonthDay2(d._year, d._month);
//		d._day += days;
//		d._month--;
//		if (d._month == 0)
//		{
//			d._year--;
//			d._month = 12;
//		}
//	}
//	return d;
//}

Date Date::operator-(int day)const//运算符重载的复用
{
	Date ret = *this;
	ret -= day;
	return ret;
}

int Date::operator-(const Date& d) const//日期-日期=天数
{
	//进行了很多运算符的复用
	int flag = 1;
	Date max = *this;
	Date min = d;
	if (*this < d)
	{
		max = d;
		min = *this;
		flag = -1;
	}
	int n = 0;
	while (min != max)
	{
		min++;
		n++;
	}
	return n * flag;
}

Date& Date::operator--()//前置--
{
	*this -= 1;
	return *this;
}

Date Date::operator--(int)//后置--
{
	Date tmp(*this);
	*this -= 1;
	return tmp;
}

ostream& operator<<(ostream& out, const Date& d)
{
	out << d._year << "/" << d._month << "/" << d._day << endl;

	return out;
}

istream& operator>>(istream& in, Date& d)
{
	in >> d._year >> d._month >> d._day;
	return in;
}

