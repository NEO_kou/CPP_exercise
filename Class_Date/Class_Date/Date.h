#pragma once

#include<iostream>
#include<assert.h>
using namespace std;
class Date
{
	friend ostream& operator<<(ostream& out, const Date& d);//使用友元函数解决类外函数不能访问类中私密成员的问题
	friend istream& operator>>(istream& in, Date& d);

public:
	Date* operator&()//自己不写系统会自动生成一个
	{
		return this;
		//如果不想让人得到地址: return nullptr;
	}
	//构造函数会频繁调用,放在类中作为inline
	Date(int year = 1, int month = 1, int day = 1)//构造函数
	{
		_year = year;
		_month = month;
		_day = day;
		if (_month > 12 && _day > GetMonthDay(_year, _month))
			_legal = false;
	}

	Date& operator=(const Date& d)//传参和返回用引用
	{
		if (this != &d)//保证不自己给自己赋值
		{
			_year = d._year;
			_month = d._month;
			_day = d._day;
		}
		return *this;
	}

	bool CheckDate()//检查日期是否正确
	{
		if (_year >= 1
			&& _month > 0 && _month < 13
			&& _day > 0 && _day <= GetMonthDay(_year, _month))
		{
			return true;
		}
		else
		{
			return false;
		}
	}


	int GetMonthDay(int year, int month)
	{
		static int day[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };//将数组放在静态区,调用次数多了也不会很消耗栈帧
		if (month == 2
			&& ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)))
		{
			return 29;
		}

		else
		{
			return day[month];
		}
	}

	int GetMonthDay2(int year, int month)
	{
		static int day[13] = { 0,31,31,28,31,30,31,30,31,31,30,31,30 };//将数组放在静态区,调用次数多了也不会很消耗栈帧
		if (month == 3
			&& ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)))
		{
			return 29;
		}

		else
		{
			return day[month];
		}
	}

	void Print()const;

	void Print();//和上面构成函数重载!

	bool operator==(const Date& d)const;

	bool operator!=(const Date& d)const;

	bool operator>(const Date& d)const;

	bool operator>=(const Date& d)const;

	bool operator<(const Date& d)const;

	bool operator<=(const Date& d)const;

	Date& operator+=(int day);

	Date operator+(int day)const;// 日期+天数=日期

	Date& operator++();//前置++,可以用引用返回

	Date operator++(int);//后置++,用值返回

	Date operator-(int day)const;// 日期-天数=日期

	int operator-(const Date& d)const; //日期-日期=天数

	Date& operator-=(int day);

	Date& operator--();//前置--

	Date operator--(int);//后置--
	bool _legal = true;
private:
	int _year;
	int _month;
	int _day;
};


//// 流插入重载
//inline ostream& operator<<(ostream& out, const Date& d)
//{
//	out << d._year << "年" << d._month << "月" << d._day << "日" << endl;
//	return out;
//}
//
//// 流提取重载
//inline istream& operator>>(istream& in, Date& d)
//{
//	in >> d._year >> d._month >> d._day;
//	assert(d.CheckDate());
//	return in;
//}

ostream& operator<<(ostream& out, const Date& d);
istream& operator>>(istream& in, Date& d);

