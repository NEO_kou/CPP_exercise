#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable:6031)

#include"Date.h"


//void Test1()
//{
//	Date d1(2023, 6, 19);
//	//Date& d2 = d1;  如果使用引用定义d2,那么改变d2会影响d1
//	Date d2 = d1;//这时调用构造函数
//	Date d3(2003, 1, 22);
//    //这时是赋值运算.
//	d2 = d1 = d3; //d2 = d1.operator=(&d1,d3)
//	d2._year = 555;//这时d1不会改变
//
//}

//void Test2()
//{
//	Date d1(2023, 6, 19);
//	Date d2(2003, 1, 22);
//	cout << (d1 <= d2) << endl;
//}

//void Test3()
//{
//	Date d1(2023, 6, 19);
//	d1 += 5000;
//	d1.Print();
//	
//}

//void Test4()
//{
//	Date d1(2023, 6, 19);
//	d1++;
//	d1.Print();
//	Date d2(2003, 1, 22);
//	(++d2).Print();
//	Date d3(2055, 5, 5);
//	(d3++).Print();
//}

//void Test5()
//{
//	Date d1(2023, 6, 20);
//	Date d2 = d1 - 5000;
//	d2.Print();
//	d2--;
//	--d2;
//	d2.Print();
//	d1 -= 50;
//	d1.Print();
//}

//void Test6()
//{
//	Date d1(2023, 6,20);
//	d1.Print();
//	Date d2(2023, 13, 1);//非法日期
//	Date d3(2023, 6, 32);
//}

void Test7()
{
	Date d1(2023, 6, 20);
	//cout << d1;
	cout << d1;
	//d1.operator<<(cout);
	Date d2(2023, 5, 25);
	cout << d1 << d2 << endl;
}

void Test8()
{
	Date d1(2023, 6, 20);
	Date d2;
	cin >> d2;
	cout << d2;
}

void Test9()
{
	Date d1(2023, 6, 21);
	const Date d2(2023, 7, 9);
	d2.Print();
}

void CutString(string& in, string& oldyear, string& oldmonth, string& oldday, string& newyear, string& newmonth, string& newday, const string& sep)
{
    int pos1 = in.find(sep, 0);
    if (pos1 != string::npos)
        oldyear = in.substr(0, pos1);
    int pos2 = in.find(sep, pos1 + 1);
    if (pos2 != string::npos)
        oldmonth = in.substr(pos1 + sep.size(), pos2 - pos1 - 1);
    int pos3 = in.find(sep, pos2 + 1);
    if (pos3 != string::npos)
        oldday = in.substr(pos2 + sep.size(), pos3 - pos2 - 1);
    int pos4 = in.find(sep, pos3 + 1);
    if (pos4 != string::npos)
        newyear = in.substr(pos3 + sep.size(), pos4 - pos3 - 1);
    int pos5 = in.find(sep, pos4 + 1);
    if (pos5 != string::npos)
        newmonth = in.substr(pos4 + sep.size(), pos5 - pos4 - 1);
    newday = in.substr(pos5 + sep.size());
}

void CutString(std::string& in, const std::string& sep, std::string& out1, std::string& out2)
{
    auto pos = in.find(sep);
    if (std::string::npos != pos) {
        out1 = in.substr(0, pos);
        out2 = in.substr(pos + sep.size());
    }
}

int main()
{
    string query_string;
    cin >> query_string;
    //GetQueryString(query_string);
    string oldyear, oldmonth, oldday, newyear, newmonth, newday;
    CutString(query_string, oldyear, oldmonth, oldday, newyear, newmonth, newday, "&");
    string oy, om, od, ny, nm, nd, tmp;
    CutString(oldyear, "=", tmp, oy);
    CutString(oldmonth, "=", tmp, om);
    CutString(oldday, "=", tmp, od);
    CutString(newyear, "=", tmp, ny);
    CutString(newmonth, "=", tmp, nm);
    CutString(newday, "=", tmp, nd);
    Date old(atoi(oy.c_str()), atoi(om.c_str()), atoi(od.c_str()));
    Date now(atoi(ny.c_str()), atoi(nm.c_str()), atoi(nd.c_str()));
    if (old._legal == false || now._legal == false)
        cout << "日期非法";
    else
    {
        int ret = now - old;
        cout << ret;
    }

}

