#pragma once
#include<iostream>
using namespace std;

struct QueueNode
{
	struct QueueNode* next;
	int val;
};

class Queue
{
public:
	void Init();
	void Pop();
private:
	void Push(int x);
	QueueNode* head;
	QueueNode* tail;
};