#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable:6031)

#include<iostream>
#include<stdio.h>
#include<vector>
#include<algorithm>
#include<stack>
using namespace std;
class MyCircularQueue {
public:
    MyCircularQueue(int k) {
        a = new int[k + 1];
        front = 0;
        rear = 0;
        _k = k;
    }

    bool enQueue(int value) {
        if (isFull())
        {
            return false;
        }
        else {
            a[rear] = value;
            rear++;
            rear %= _k + 1;
            return true;
        }
    }

    bool deQueue() {
        if (isEmpty())
        {
            return false;
        }
        else {
            front++;
            front %= _k + 1;
            return true;
        }
    }

    int Front() {
        if (isEmpty())
        {
            return -1;
        }
        else {
            return a[front];
        }
    }

    int Rear() {
        if (isEmpty())
        {
            return -1;
        }
        else {
            return a[(rear + _k) % (_k + 1)];
        }
    }

    bool isEmpty() {
        return front == rear;
    }

    bool isFull() {
        return (rear + 1) % (_k + 1) == front;
    }

private:
    int front;
    int rear;
    int _k = 0;
    int* a;
};


class MinStack {//力扣155,最小栈
public:
    MinStack() {//有没有都可以通过

    }

    void push(int val) {
        _st.push(val);
        if (_min.empty() || val <= _min.top())
        {
            _min.push(val);
        }
    }

    void pop() {
        if (_st.empty())
        {
            return;
        }
        if (_st.top() == _min.top())//要在st删除前判断
        {
            _min.pop();
        }
        _st.pop();
    }

    int top() {
        return _st.top();
    }

    int getMin() {
        return _min.top();
    }

private:
    stack<int> _st;//不用写构造函数,默认的构造会调用stack的构造
    stack<int> _min;
};

class Solution {//压栈,出栈匹配问题
public:

    bool IsPopOrder(vector<int>& pushV, vector<int>& popV) {
        stack<int> st;
        int j = 0;
        for (auto e : pushV)//每次都入栈一个元素
        {
            st.push(e);

            while (!st.empty() && popV[j] == st.top())
            {
                j++;
                st.pop();
            }
        }

        return st.empty();//当走完所有的push栈后,st为空就不匹配
    }
};

int main()
{
    MyCircularQueue* circularQueue = new MyCircularQueue(3);

    return 0;
}