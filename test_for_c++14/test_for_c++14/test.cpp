#include<iostream>
#include<string>
#include<map>
using namespace std;

int main()
{
	std::string str = "hello!";
	// ret的值为：hello
	// str的值为：hello world!
	auto ret = std::exchange(str, "hello world!");
	cout << "str: " << str << endl;
	cout << "ret: " << ret << endl;

	int a = 0b1001;
	int b = 1234'5678;
	cout << a << endl;
	cout << b << endl;

	std::map<std::string, int> my_map;
	std::tuple<int, std::string, const char*> my_tuple;
	for (const auto& [key, value] : my_map) {
		// do something
	}
	auto [1, "1", nullprt] = my_tuple;

	return 0;
}