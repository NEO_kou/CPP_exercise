class Solution {
public:
    string reverseWords(string s) {
        vector<string> tmpret;
        while (s != "")
        {
            int n = s.size();
            int i = n - 1;
            while (i >= 0 && s[i] == ' ')
                i--;
            while (i >= 0 && s[i] != ' ')
                i--;
            string tmpstr = s.substr(i + 1);
            while (tmpstr.size() > 0 && tmpstr[tmpstr.size() - 1] == ' ')
                tmpstr.erase(tmpstr.size() - 1);
            tmpret.push_back(tmpstr);
            s.erase(i + 1);
        }
        string ret;
        for (auto& str : tmpret)
        {
            ret += str;
            ret += ' ';
        }
        while (ret[ret.size() - 1] == ' ')
            ret.erase(ret.size() - 1);
        return ret;
    }
};