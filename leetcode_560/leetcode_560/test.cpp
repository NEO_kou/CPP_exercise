class Solution {
public:
    int subarraySum(vector<int>& nums, int k) {
        int n = nums.size();
        int ret = 0;
        for (int i = 0; i < n; i++)
        {
            int sum = nums[i];
            bool flag = true;
            if (sum == k)
            {
                ret++;
                flag = false;
            }
            for (int j = i - 1; j >= -1; j--)
            {
                if (sum == k && flag)
                {
                    ret++;
                }
                flag = true;
                if (j < 0) break;
                else sum += nums[j];
            }
        }
        return ret;
    }
};