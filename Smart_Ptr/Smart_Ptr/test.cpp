#define _CRT_SECURE_NO_WARNINGS 1
#pragma warning(disable:6031)
#include"smart.h"

struct Date
{
	int _year = 0;
	int _month = 0;
	int _day = 0;
	Date()
	{}
};
void SharePtrFunc(Smart_Ptr<Date>& sp, size_t n, mutex& mtx)
{
	cout << sp.get() << endl;
	for (size_t i = 0; i < n; ++i)
	{
		// 这里智能指针拷贝会++计数，智能指针析构会--计数，这里是线程安全的。
		Smart_Ptr<Date> copy(sp);
		// 这里智能指针访问管理的资源，不是线程安全的。所以我们看看这些值两个线程++了2n次，但是最终看到的结果，并一定是加了2n
		{
			mtx.lock();
			copy->_year++;
			copy->_month++;
			copy->_day++;
			mtx.unlock();
		}
	}
}


void test_shared()
{
	Smart_Ptr<Date> p(new Date());
	cout << p.get() << endl;
	const size_t n = 100000;
	mutex mtx;
	thread t1(SharePtrFunc, std::ref(p), n, std::ref(mtx));
	thread t2(SharePtrFunc, std::ref(p), n, std::ref(mtx));
	t1.join();
	t2.join();
	cout << p->_year << endl;
	cout << p->_month << endl;
	cout << p->_day << endl;
	cout << p.use_count() << endl;
}

struct ListNode
{
	std::weak_ptr<ListNode> _next;
	std::weak_ptr<ListNode> _prev;
	int _val;
	~ListNode() { cout << "~ListNode()" << endl; }
};

void test_shared_cycle()
{
	/*ListNode* n1(new ListNode());//有抛异常问题,使用智能指针
	ListNode* n2(new ListNode());
	n1->_next = n2;
	n2->_prev = n1;
	delete n1;
	delete n2;*/

	//Smart_Ptr<ListNode> node1(new ListNode);
	//Smart_Ptr<ListNode> node2(new ListNode);
	shared_ptr<ListNode> node1(new ListNode);
	shared_ptr<ListNode> node2(new ListNode);
	cout << node1.use_count() << endl;
	cout << node2.use_count() << endl;
	(*node1)._next = node2;
	(*node2)._prev = node1;
	cout << node1.use_count() << endl;
	cout << node2.use_count() << endl;
}

int main()
{
	test_shared_cycle();
	return 0;
}