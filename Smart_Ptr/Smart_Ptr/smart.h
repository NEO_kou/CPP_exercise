#pragma once
#include<iostream>
#include<thread>
#include<memory>
#include<mutex>
#include<cstdio>
using namespace std;

template<class T>
class Smart_Ptr //实现的C++11的shared_ptr版本
{
public:
	/*Smart_Ptr(const T*& ptr)
		:_ptr(ptr)
		,_pcount(new int(1))
	{ }*/

	Smart_Ptr(T* ptr = nullptr)
		:_ptr(ptr)
		,_pcount(new int(1))
		,_pmtx(new mutex())
	{}

	~Smart_Ptr()
	{
		Release();
	}

	Smart_Ptr(const Smart_Ptr<T>& sp)
		:_ptr(sp._ptr)
		,_pcount(sp._pcount)
		,_pmtx(sp._pmtx)
	{
		Addcount();
	}

	Smart_Ptr<T>& operator=(const Smart_Ptr<T>& sp)
	{
		if (_ptr != sp._ptr)
		{
			Release();
			_ptr = sp._ptr;
			_pcount = sp._pcount;
			_pmtx = sp._pmtx;
			Addcount();
		}
		return *this;
	}

	void Release()
	{
		_pmtx->lock();
		if (--(*_pcount) == 0)//销毁最后一个变量时才释放资源
		{
			delete _ptr;
			delete _pcount;
			_pmtx->unlock();
			delete _pmtx;
		}
		else _pmtx->unlock();
	}

	void Addcount()
	{
		_pmtx->lock();
		(*_pcount)++;
		_pmtx->unlock();
	}

	void Subcount()
	{
		Release();
	}

	T& operator*()
	{
		return *_ptr;
	}
	T* operator->()
	{
		return _ptr;
	}
	T* get() const
	{
		return _ptr;
	}
	int use_count()
	{
		return *_pcount;
	}
private:
	T* _ptr;
	int* _pcount;
	mutex* _pmtx;
};


template<class T>
class Weak_Ptr
{
public:
	Weak_Ptr()
		:_ptr(nullptr)
	{}
	Weak_Ptr(const shared_ptr<T>& sp)
		:_ptr(sp.get())
	{}
	Weak_Ptr(const Smart_Ptr<T>& sp)
		:_ptr(sp.get())
	{}
	T& operator*()
	{
		return *_ptr;
	}
	T* operator->()
	{
		return _ptr;
	}
	T* get()
	{
		return _ptr;
	}
private:
	T* _ptr;
};
