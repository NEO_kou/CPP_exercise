#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable:6031)

#include<iostream>

using namespace std;
//
//template<typename T>// T为模板类型,可取任意名字
//
////template<class T>  //这里使用typename和class是一样的意思
//void Swap(T& x, T& y)
//{
//	T tmp = x;
//	x = y;
//	y = tmp;
//}

template<class T>
T Add(const T& x, const T& y)
{
	return x + y;
}

int Add(int& x, int& y)//可以和模板函数同时存在,优先调用此函数
{
	int tmp = x;
	x = y;
	y = tmp;
}
//template<class T1,class T2>
//T1 Add(const T1& x, const T2& y)
//{
//	return x + y;
//}
int main()
{
	/*int a1 = 1;
	int b1 = 2;
	double a2 = 5.5;
	double b2 = 3.3;
	char a3 = 'p';
	char b3 = 'q';
	swap(a1, b1);
	swap(a2, b2);
	swap(a3, b3);*/

	//cout << Add(1, 2) << endl;
	//cout << Add(5.5, 3) << endl;//推演实例化时报错,遇见5.5会将T变成double类型,遇见3又变成int类型
	//cout << Add<int>(5.5, 3) << endl;
	//cout << Add<double>(5.5, 3) << endl;
	//cout << Add<double,int>(5.5, 3) << endl;
	return 0;
}