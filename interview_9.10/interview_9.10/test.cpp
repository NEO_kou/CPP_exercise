#include<iostream>
#include<vector>
#include<mutex>
#include<thread>
#include<string>
#include<algorithm>
#include<stack>
#include<queue>
#include<cstdio>
#include <condition_variable>
#include<functional>
#include<cmath>

using namespace std;
/// //////////////////////////////////////////////////////////////////////////////////////////////////////////
// 快排
void _QuickSort(vector<int>& v,int left,int right,int key)
{
	if (left >= right) return;
	int tmpl = left;
	int tmpr = right;
	while (tmpl < tmpr)
	{
		while (tmpl < tmpr && v[tmpr] >= v[key])
			tmpr--;
		while (tmpl < tmpr && v[tmpl] <= v[key])
			tmpl++;
		swap(v[tmpl], v[tmpr]);
	}
	swap(v[key], v[tmpl]);
	_QuickSort(v, left, tmpl - 1, left);
	_QuickSort(v, tmpl + 1, right, tmpl + 1);
}

void QuickSort(vector<int>& v)
{
	int n = v.size();
	_QuickSort(v, 0, n - 1, 0);
}
/// //////////////////////////////////////////////////////////////////////////////////////////////////////////
//归并排序
void _MergeSort(vector<int>& v,int left,int right,vector<int>& tmp)
{
	if (left >= right) return;
	int mid = (left + right) / 2;
	_MergeSort(v, left, mid, tmp);
	_MergeSort(v, mid + 1, right, tmp);
	int begin1 = left;
	int end1 = mid;
	int begin2 = mid + 1;
	int end2 = right;
	int index = left;
	while (begin1 <= end1 && begin2 <= end2)
	{
		if (v[begin1] <= v[begin2])
			tmp[index++] = v[begin1++];
		else
			tmp[index++] = v[begin2++];
	}
	while(begin1<=end1)
		tmp[index++] = v[begin1++];
	while(begin2<=end2)
		tmp[index++] = v[begin2++];
	for (int i = left; i <= right; i++)
		v[i] = tmp[i];
}

void MergeSort(vector<int>& v)
{
	vector<int> tmp = v;
	_MergeSort(v, 0, v.size() - 1, tmp);
}
/// //////////////////////////////////////////////////////////////////////////////////////////////////////////
//二叉树的前中后序遍历(非递归)
struct TreeNode {
	int val;
	TreeNode* left;
	TreeNode* right;
	TreeNode() : val(0), left(nullptr), right(nullptr) {}
	TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
	TreeNode(int x, TreeNode* left, TreeNode* right) : val(x), left(left), right(right) {}
};

vector<int>& PreOrder(TreeNode* root)
{
	vector<int> ret;
	stack<TreeNode*> st;
	TreeNode* cur = root;
	while (cur || !st.empty())
	{
		while (cur)
		{
			ret.push_back(cur->val);
			st.push(cur);
			cur = cur->left;
		}
		TreeNode* tmp = st.top();
		cur = tmp->right;
		st.pop();
	}
	return ret;
}

vector<int>& InOrder(TreeNode* root)
{
	vector<int> ret;
	stack<TreeNode*> st;
	TreeNode* cur = root;
	while (cur || !st.empty())
	{
		while (cur)
		{
			st.push(cur);
			cur = cur->left;
		}
		TreeNode* tmp = st.top();
		ret.push_back(tmp->val);
		st.pop();
		cur = tmp->right;
	}
}

vector<int>& PostOrder(TreeNode* root)
{
	vector<int> ret;
	stack<TreeNode*> st;
	TreeNode* cur = root;
	TreeNode* prev = nullptr;
	while (cur || !st.empty())
	{
		while (cur)
		{
			st.push(cur);
			cur = cur->left;
		}
		TreeNode* top = st.top();
		if (top->right == nullptr || top->right == prev)
		{
			ret.push_back(top->val);
			st.pop();
			prev = top;
		}
		else cur = top->right;
	}
}
/// //////////////////////////////////////////////////////////////////////////////////////////////////////////
// 单例模式
class Singleton
{
public:
	static Singleton* GetInstance()
	{
		if (_sing == nullptr)
		{
			_mtx.lock();
			if (_sing == nullptr)
				_sing = new Singleton();
			_mtx.unlock();
		}
		return _sing;
	}
private:
	Singleton(){}
	Singleton(const Singleton& s) = delete;
private:
	static Singleton* _sing;
	static mutex _mtx;
};

Singleton* Singleton::_sing = nullptr;
mutex Singleton::_mtx;
/// //////////////////////////////////////////////////////////////////////////////////////////////////////////
// 智能指针
template<class T>
class SharedPtr
{
public:
	SharedPtr(T* ptr = nullptr):_ptr(ptr),_count(new int(1)),_mtx(new mutex)
	{}
	SharedPtr(const SharedPtr& sp)
	{
		sp._count = _count;
		sp._mtx = _mtx;
		sp._ptr = _ptr;
		AddCount();
	}
	~SharedPtr()
	{
		Release();
	}
	void Release()
	{
		(*_mtx).lock();
		if (--(*_count) == 0)
		{
			delete _ptr;
			delete _count;
			delete _mtx;
		}
		(*_mtx).unlock();
	}
	SharedPtr<T> operator=(const SharedPtr<T>& sp)
	{
		if (sp._ptr != _ptr)
		{
			Release();
			_ptr = sp._ptr;
			_mtx = sp._mtx;
			_count = sp._count;
			AddCount();
		}
		return this;
	}
	void AddCount()
	{
		_mtx->lock();
		(*count)++;
		_mtx->unlock();
	}
	T& operator*()
	{
		return *_ptr;
	}
	T* operator->()
	{
		return _ptr;
	}
private:
	T* _ptr;
	int* _count;
	mutex* _mtx;
};
/// //////////////////////////////////////////////////////////////////////////////////////////////////////////
// 线程池
mutex MTX;
using func_t = function<void(int)>;
class Task
{
	void Run()
	{
		cout << "hello world" << endl;
	}
};
class ThreadPool
{
	static ThreadPool* GetInstance()
	{
		if (_single == nullptr)
		{
			MTX.lock();
			if (_single == nullptr)
			{
				_single = new ThreadPool();
			}
			MTX.unlock();
		}
		return _single;
	}
	void Start()
	{
		for (int i = 0; i < _num; i++)
		{
			thread t(Routine,this);
			_v.push_back(t);
		}
		for (int i = 0; i < _num; i++)
		{
			_v[i].join();
		}
	}
	static void Routine(void* args)
	{
		ThreadPool* td = (ThreadPool*)args;
		while (1)
		{
			Task t;
			td->_mtx.lock();
			while (td->empty())
			{
				_cond.wait(td->_mtx);
			}
			t = td->Pop();
			td->_mtx.unlock();
		}
	}
	void Push(Task& t)
	{
		_mtx.lock();
		_q.push(t);
		_mtx.unlock();
		_cond.notify_one();
	}
	Task& Pop()
	{
		Task tmp = _q.front();
		_q.pop();
		return tmp;
	}
	bool empty()
	{
		if (_q.size() == 0) return true;
		else return false;
	}
private:
	ThreadPool():_num(6)
	{
		_v.resize(_num);
	}
	ThreadPool(const ThreadPool& tp) = delete;
private:
	queue<Task> _q; //阻塞队列
	int _num; //线程的数量
	vector<thread> _v;
	mutex _mtx; //互斥锁
	static condition_variable _cond;
	static ThreadPool* _single;
};

ThreadPool* ThreadPool::_single = nullptr;

int main()
{
	vector<int> v{ 3,3,1,2,3,3,2,1,5,2,6,6,6,2,2 };
	MergeSort(v);
	for (auto& x : v)
		cout << x << " ";
	return 0;
}