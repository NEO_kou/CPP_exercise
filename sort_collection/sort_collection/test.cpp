#include<iostream>
#include<vector>
#include<string>
#include<memory>
using namespace std;

void Print(vector<int>& v1,vector<int>& v2)
{
	for (auto x1 : v1)
		cout << x1 << " ";
	cout << endl;
	for (auto x2 : v2)
		cout << x2 << " ";
	cout << endl;
}

void BubbleSort(vector<int>& v)
{
	if (v.size() == 1 || v.size() == 0) return;
	int n = v.size();
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n-1; j++)
		{
			if (v[j] > v[j + 1])
			{
				int tmp = v[j];
				v[j] = v[j + 1];
				v[j + 1] = tmp;
			}
		}
	}
}

void SelectSort(vector<int>& v)
{
	if (v.size() == 1 || v.size() == 0) return;
	int n = v.size();
	int index = 0;
	while (index < n / 2)
	{
		int maxi = index; int mini = index;
		for (int i = index; i < n-index-1; i++)
		{
			if (v[i] > v[maxi]) maxi = i;
			if (v[i] < v[mini]) mini = i;
		}
		int prev = index;
		int tail = n - index - 1;
		swap(v[mini], v[prev]);
		swap(v[maxi], v[tail]);
		index++;
	}
	
}

void InsertSort(vector<int>& v)
{
	if (v.size() == 1 || v.size() == 0) return;
	int n = v.size();
	for (int i = 1; i < n; i++)
	{
		int end = i;
		while (end - 1 >= 0 && v[end] <= v[end - 1])
		{
			swap(v[end - 1], v[end]);
			end--;
		}
	}
}

void _QuickSort(vector<int>& v, int left, int right, int key)
{
	if (left >= right) return;
	int keyvalue = v[key];
	int begin = left; int end = right;
	while (begin < end)
	{
		while (begin < end && v[end] > keyvalue) end--;
		while (begin < end && v[begin] <= keyvalue) begin++;
		swap(v[begin], v[end]);
	}
	swap(v[key], v[begin]);
	_QuickSort(v, left, begin - 1, left);
	_QuickSort(v, begin + 1, right, begin + 1);
}

void QuickSort(vector<int>& v)
{
	if (v.size() == 1 || v.size() == 0) return;
	_QuickSort(v, 0, v.size() - 1, 0);
}

void _MergeSort(vector<int>& v, int left, int right,vector<int> tmp)
{
	if (left >= right) return;
	int mid = (left + right) / 2;
	_MergeSort(v, left, mid, tmp);
	_MergeSort(v, mid + 1, right, tmp);
	int begin1 = left; int end1 = mid;
	int begin2 = mid + 1; int end2 = right;
	int i = left;
	while (begin1 <= end1 && begin2 <= end2)
	{
		if (v[begin1] <= v[begin2])
			tmp[i++] = v[begin1++];
		else tmp[i++] = v[begin2++];
	}
	while (begin1 <= end1)
		tmp[i++] = v[begin1++];
	while (begin2 <= end2)
		tmp[i++] = v[begin2++];
	for (int j = left; j <= right; j++)
		v[j] = tmp[j];
}

void MergeSort(vector<int>& v)
{
	if (v.size() == 1 || v.size() == 0) return;
	vector<int> tmp = v;
	_MergeSort(v, 0, v.size() - 1, tmp);
}

void AdjustDown(vector<int>& v, int parent, int n)
{
	int child = parent * 2 + 1;
	while (child < n)
	{
		if (child + 1 < n && v[child] < v[child + 1])
			child++;
		if (v[parent] < v[child])
		{
			swap(v[parent], v[child]);
			parent = child;
			child = parent * 2 + 1;
		}
		else break;
	}
}

void HeapSort(vector<int>& v)
{

}

int main()
{
	vector<int> v1{ 4,7,1,6,9,8,2,5,3 };
	vector<int> v2{ 2,2,2,9,4,9,6,9,3,3,3,1,7,1 };
	MergeSort(v1);
	MergeSort(v2);
	Print(v1, v2);
	return 0;
}