class Solution {
public:
    bool hash_ju(int* p, int* q)
    {
        for (int i = 0; i < 26; i++)
        {
            if (p[i] != q[i])
            {
                return false;
            }
        }
        return true;
    }
    vector<int> findAnagrams(string s, string p) {
        if (s.size() < p.size())
        {
            vector<int> vv;
            return vv;
        }
        vector<int> ret;
        int hash1[26] = { 0 };
        for (auto ch : p)
        {
            hash1[ch - 'a']++;
        }
        int hash2[26] = { 0 };
        int lenp = p.size();
        int left = 0, right = 0;
        while (right < s.size())
        {
            while (right < lenp)
            {
                hash2[s[right] - 'a']++;
                right++;
            }
            if (right >= s.size())
            {
                break;
            }
            if (hash_ju(hash1, hash2) == true)
            {
                ret.push_back(left);
            }
            hash2[s[left] - 'a']--;
            hash2[s[right] - 'a']++;
            left++;
            right++;
        }
        if (hash_ju(hash1, hash2) == true)
        {
            ret.push_back(left);
        }
        return ret;
    }
};