#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable:6031)
#include"List.h"


//void test1()
//{
//	list<int> lt;
//	lt.push_back(1);
//	lt.push_back(2);
//	lt.push_back(3);
//	lt.push_back(4);
//	lt.push_back(5);
//	lt.push_back(6);
//
//	list<int>::iterator it1 = lt.begin();
//	while (it1 != lt.end())
//	{
//		cout << *it1 << " ";
//		it1++;
//	}
//	cout << endl;
//
//	lt.push_front(66);
//	lt.push_front(77);
//	lt.push_front(88);
//	lt.pop_back();
//	lt.pop_back();
//	list<int>::iterator it2 = lt.begin();
//	while (it2 != lt.end())
//	{
//		cout << *it2 << " ";
//		it2++;
//	}
//	cout << endl;
//}
//
//void test2()
//{
//	list<int> lt;
//	lt.push_back(1);
//	lt.push_back(2);
//	lt.push_back(3);
//	lt.push_back(4);
//	lt.push_back(5);
//	lt.push_back(6);
//
//	list<int>::iterator pos = find(lt.begin(), lt.end(), 3);
//	lt.insert(pos, 30);
//	list<int>::iterator it1 = lt.begin();
//	while (it1 != lt.end())
//	{
//		cout << *it1 << " ";
//		it1++;
//	}
//	cout << endl;
//
//	lt.insert(pos, 40);
//	list<int>::iterator it2 = lt.begin();//inser插入后,不会出现迭代器失效问题
//	while (it2 != lt.end())
//	{
//		cout << *it2 << " ";
//		it2++;
//	}
//	cout << endl;
//	*pos *= 20;
//	list<int>::iterator it3 = lt.begin();
//	while (it3 != lt.end())
//	{
//		cout << *it3<< " ";
//		it3++;
//	}
//}
//
//void test3()
//{
//	list<int> lt;
//	lt.push_back(100);
//	lt.push_back(20);
//	lt.push_back(350);
//	lt.push_back(40);
//	lt.push_back(500);
//	lt.push_back(620);
//
//	auto pos = find(lt.begin(), lt.end(), 40);
//	if (pos != lt.end())
//	{
//		lt.erase(pos);
//	}
//	else
//	{
//		printf("要删除的数据不存在");
//		return;
//	}
//	list<int>::iterator it1 = lt.begin();
//	while (it1 != lt.end())
//	{
//		cout << *it1 << " ";
//		it1++;
//	}
//}

void test1()
{
	List<int> lt;
	lt.push_back(1);
	lt.push_back(2);
	lt.push_back(3);
	lt.push_back(4);
	lt.push_back(5);
	for (auto e : lt)
	{
		cout << e << " ";
	}
	cout << endl;
	auto it = lt.begin();
	++it;
	while (it != lt.end())
	{
		cout << *it << " ";
		++it;
	}

}

struct neo
{
	int a1;
	int a2;

	neo(int aa1 = 0, int aa2 = 0)
		:a1(aa1)
		,a2(aa2)
	{

	}
};

void test2()
{
	List<neo> lt;
	lt.push_back(neo(10, 20));
	lt.push_back(neo(33, 66));
	lt.push_back(neo(22, 44));
	lt.push_back(neo(88, 99));
	List<neo>::iterator it = lt.begin();

	while (it != lt.end())//没有运算符重载->时可以这样使用
	{
		cout << (*it).a1 << "--" << (*it).a2 << endl;
		it++;
	}

	cout << endl;
	List<neo>::iterator it1 = lt.begin();
	while (it1 != lt.end())//重载了运算符时这样使用
	{
		cout << it1 -> a1 << "--" << it1 -> a2 << endl;//实际上的it1->a1应该是it1->->a1,编译器提供了可读性进行了代码优化
		it1++;
	}
}

//void test3()
//{
//	const List<int> lt;
//	List<int>::const_iterator it = lt.begin();
//	*it = 3;
//
//}

void test4()//
{
	List<int> lt;
	lt.push_back(1);
	lt.push_back(2);
	lt.push_back(3);
	lt.push_back(4);
	lt.push_back(5);
	lt.push_back(6);
	List<int>::iterator it1 = lt.begin();
	while (it1 != lt.end())
	{
		cout << *it1 << " ";
		it1++;
	}
	cout << endl;
	lt.push_back(1);
	lt.push_back(1);
	lt.push_back(1);
	lt.push_back(1);
	lt.pop_front();
	lt.pop_front();
	List<int>::iterator it2 = lt.begin();
	while (it2 != lt.end())
	{
		cout << *it2 << " ";
		it2++;
	}
	cout << endl;
	auto pos = ++(lt.begin());
	lt.insert(pos, 33);
	pos++;
	lt.insert(pos, 44);
	pos++;
	lt.insert(pos, 55);
	List<int>::iterator it3 = lt.begin();
	while (it3 != lt.end())
	{
		cout << *it3 << " ";
		it3++;
	}
}

void test5()//find函数的测试
{
	List<int> lt;
	lt.push_back(1);
	lt.push_back(2);
	lt.push_back(3);
	lt.push_back(4);
	lt.push_back(5);
	lt.push_back(6);

	auto pos = ::find(lt.begin(), lt.end(), 4);
	lt.insert(pos, 100);
	List<int>::iterator it1 = lt.begin();
	while (it1 != lt.end())
	{
		cout << *it1 << " ";
		it1++;
	}
	lt.clear();
}

void test6()//拷贝构造的测试
{
	List<int> lt;
	lt.push_back(1);
	lt.push_back(2);
	lt.push_back(3);
	lt.push_back(4);
	lt.push_back(5);
	lt.push_back(6);

	List<int> lt2 = lt;
	lt2.pop_back();
	lt2.push_front(100);
	List<int>::iterator it1 = lt.begin();
	while (it1 != lt.end())
	{
		cout << *it1 << " ";
		it1++;
	}
	cout << endl;
	List<int>::iterator it2 = lt2.begin();
	while (it2 != lt2.end())
	{
		cout << *it2 << " ";
		it2++;
	}
}

void test7()//重载符=的测试
{
	List<int> lt;
	lt.push_back(1);
	lt.push_back(2);
	lt.push_back(3);
	lt.push_back(4);
	lt.push_back(5);
	lt.push_back(6);

	List<int> lt2;
	lt2.push_back(1);
	lt2.push_back(2);
	lt2.push_back(3);

	lt2 = lt;
	lt2.push_back(100);

	List<int>::iterator it1 = lt.begin();
	while (it1 != lt.end())
	{
		cout << *it1 << " ";
		it1++;
	}
	cout << endl;
	List<int>::iterator it2 = lt2.begin();
	while (it2 != lt2.end())
	{
		cout << *it2 << " ";
		it2++;
	}

}

int main()
{
	//test3();
	test7();
	return 0;
}