class Solution {
public:
    int lengthOfLongestSubstring(string s) {
        int n = s.size();
        if (n <= 1) return n;
        int index = -1;
        int ret = 1;
        for (int i = 0; i < n; i++)
        {
            map<char, int> mp;
            mp[s[i]] = i;
            bool flag = true;
            for (int j = i + 1; j < n; j++)
            {
                if (mp.find(s[j]) != mp.end())
                {
                    ret = max(ret, j - i);
                    i = mp[s[j]];
                    flag = false;
                    break;
                }
                else
                {
                    mp[s[j]] = j;
                }
            }
            if (flag) ret = max(ret, n - i);
        }
        return ret;
    }
};