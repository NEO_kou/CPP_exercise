#include<iostream>
#include<mutex>
#include<string>
using namespace std;

class Singleton
{
private:
	Singleton()
	{

	}
	Singleton(const Singleton& s) = delete;
public:
	static Singleton* GetInstance()
	{
		if (_single == nullptr)
		{
			_mtx.lock();
			if (_single == nullptr)
			{
				_single = new Singleton();
			}
			_mtx.unlock();
		}
		return _single;
	}
	~Singleton()
	{}
private:
	static Singleton* _single;
	static mutex _mtx;
};

Singleton* Singleton::_single = nullptr;
mutex Singleton::_mtx;