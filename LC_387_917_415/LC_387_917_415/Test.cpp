#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable:6031)

#include<string>
#include<iostream>

using namespace std;

class Solution {//����415
public:
    string addStrings(string num1, string num2)
    {
        int end1 = num1.size() - 1;
        int end2 = num2.size() - 1;
        int next = 0;
        string addstr;
        while (end1 >= 0 || end2 >= 0)
        {
            int val1 = end1 >= 0 ? num1[end1] - '0' : 0;
            int val2 = end2 >= 0 ? num2[end2] - '0' : 0;
            int ret = val1 + val2 + next;
            next = ret >= 10 ? 1 : 0;
            addstr += ('0' + ret % 10);
            end1--;
            end2--;
        }
        if (next == 1)
        {
            addstr += '1';
        }
        reverse(addstr.begin(), addstr.end());

        return addstr;
    }
};

class Solution {//����387
public:

    int firstUniqChar(string s)
    {
        int* p = new int[256] {0};
        for (auto ch : s)
        {
            p[(int)ch]++;
        }
        for (int i = 0; i < s.size(); i++)
        {
            if (p[s[i]] == 1)
            {
                delete[] p;
                return i;
            }
        }
        delete[] p;
        return -1;
    }
};

class Solution {//����917
public:
    bool isal(char ch)
    {
        if ((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z'))
        {
            return true;
        }
        else
            return false;
    }
    string reverseOnlyLetters(string s)
    {
        int begin = 0;
        int end = s.size() - 1;
        while (begin < end)
        {
            while (!isal(s[begin]) && begin < end)
            {
                begin++;
            }
            while (!isal(s[end]) && begin < end)
            {
                end--;
            }
            swap(s[begin], s[end]);
            begin++;
            end--;
        }
        return s;
    }
};