#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable:6031)

#include<iostream>
using namespace std;

//class A
//{
//	int _a = 1;
//};
//class B :public A
//{
//	int _b = 2;
//};
//class C :public A
//{
//	int _c = 3;
//};
//class D :public B, A
//{
//	int _d = 4;
//};
//
//int main()
//{
//	D tmpd;
//	return 0;
//}

class A {
public:
	A() { p(); }
	virtual void p() { printf("A"); }
	virtual ~A() { p(); }
};
class B :public A {
public:
	B() { p(); }
	void p() { printf("B"); }
	~B() { p(); }
};
int main(int, char**) {
	A* a = new B();
	delete a;
	return 0;
}