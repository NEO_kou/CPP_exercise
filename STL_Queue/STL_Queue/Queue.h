#pragma once
#include<iostream>
#include<vector>
#include<deque>
using namespace std;

template<class T, class Container = deque<T>>
class Queue
{
public:
	void push(const T& val)
	{
		_con.push_back(val);
	}

	void pop()
	{
		_con.pop_front();
	}

	T& back()//�ɶ���д
	{
		return _con.back();
	}

	T& front()//�ɶ���д
	{
		return _con.front();
	}

	const T& front()const
	{
		return _con.front();
	}

	const T& back() const
	{
		return _con.back();
	}

	bool empty() const
	{
		return _con.empty();
	}

	size_t size() const
	{
		return _con.size();
	}

private:
	//vector<T> _con;
	Container _con;
};