#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable:6031)

#include<iostream>
using namespace std;

int main()
{
	int a = 0;
	int b = 1;
	int* p = &a;
	a + b;//右值
	//左值引用给左值取别名
	int& ref = a;
	//int& reab = (a + b);左值引用不能给右值取别名,权限放大了!
	const int& reab = (a + b);//加上const即可给右值取别名

	//右值引用给右值取别名
	int&& ref3 = (a + b);
	//int&& ref4 = a;右值引用无法直接引用左值
	//右值引用可以给move以后得左值取别名
	int&& ref4 = move(a);
	return 0;
}