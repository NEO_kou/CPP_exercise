#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable:6031)

#include<iostream>
using namespace std;

class Date
{
public:
	Date(int year = 1, int month = 1, int day = 1)
	{
		_year = year;
		_month = month;
		_day = day;

	}
	//Data(const Date d){},传值调用会无限循环.传参本身的Date d本身也是拷贝构造
	/*Date(const Date& d)
	{
		cout << "i like coding" << endl;
		_year = d._year;
		_month = d._month;
		_day = d._day;
	}*/

	bool operator==(const Date& x)
	{
		return _year == x._year
			&& _month == x._month
			&& _day == x._day;
	}

private:
	int _year = 1;
	int _month = 1;//c++11打的补丁
	int _day = 1;
};
void get1(Date d)
{

}
void get2(Date& d)
{

}
int main()
{
	//Date d1(2023, 6, 17);
	//get1(d1);
	//get2(d1);
	//Date d2(d1);
	//Date d3 = d1;
	Date d1(2023, 6, 17);
	Date d2(2023, 6, 17);
	cout << d1.operator==(d2) << endl;
	cout << (d1 == d2) << endl;
	return 0;
}