#pragma once
#include<iostream>
using namespace std;

template<class K>
struct BSTreeNode  //二叉搜索树
{
	BSTreeNode<K>* _left;
	BSTreeNode<K>* _right;
	K _key;

	BSTreeNode(const K& key)
		:_left(nullptr)
		,_right(nullptr)
		,_key(key)
	{ }
};

template<class K>
class BSTree
{
	typedef BSTreeNode<K> Node;
public:
	~BSTree()
	{
		_destory(_root);
	}

	BSTree(const BSTree<K>& t)
	{
		_root = _copy(t._root);
	}

	BSTree()//拷贝构造也是一种构造函数,不写构造和拷贝构造会默认生成一个构造
	{}

	BSTree<K>& operator=(BSTree<K> t)
	{
		swap(_root, t._root);

		return *this;
	}

	bool insert(const K& key)//左小右大
	{
		if (_root == nullptr)
		{
			_root = new Node(key);
			return true;
		}
		Node* cur = _root;
		Node* prev = nullptr;
		while (cur != nullptr)
		{
			if (cur->_key < key)
			{
				prev = cur;
				cur = cur->_right;
			}	
			else if (cur->_key > key)
			{
				prev = cur;
				cur = cur->_left;
			}
			else if (cur->_key == key)
				return false;
		}
		cur = new Node(key);
		if (prev->_key > key)
			prev->_left = cur;
		else
			prev->_right = cur;
		return true;
	}

	bool find(const K& key)
	{
		Node* cur = _root;
		while (cur != nullptr)
		{
			if (cur->_key < key)
				cur = cur->_right;
			else if (cur->_key > key)
				cur = cur->_left;
			else if (cur->_key == key)
				return true;
		}
		return false;
	}

	bool erase(const K& key)//非递归版本
	{
		Node* prev = nullptr;
		Node* cur = _root;
		while (cur != nullptr)
		{
			if (cur->_key < key)
			{
				prev = cur;
				cur = cur->_right;
			}
			else if (cur->_key > key)
			{
				prev = cur;
				cur = cur->_left;
			}
			else//开始删除
			{
				//1. 左边为空
				//2. 右边为空
				//3. 左右都不为空
				//4. 将根节点删除了
				if (cur->_left == nullptr)
				{
					if (cur == _root)
						_root = cur->_right;
					else
					{
						if (cur == prev->_left)
							prev->_left = cur->_right;
						else
							prev->_right = cur->_right;
					}
					delete cur;
					return true;
				}
				else if (cur->_right == nullptr)
				{
					if (cur == _root)
						_root = cur->_left;
					else
					{
						if (cur == prev->_left)
							prev->_left = cur->_left;
						else
							prev->_right = cur->_left;
					}
					delete cur;
					return true;
				}
				else//左右都不为空,使用替换法删除,统一使用右树的最小节点
				{
					Node* tmp = cur->_right;
					Node* prevtmp = cur;
					while (1)
					{
						if (tmp->_left != nullptr)
						{
							prevtmp = tmp;
							tmp = tmp->_left;
						}
						else
							break;
					}
					cur->_key = tmp->_key;
					if (tmp->_right == nullptr)
					{
						if (prevtmp == cur)
							prevtmp->_right = nullptr;
						else
							prevtmp->_left = nullptr;
						delete tmp;
						tmp = nullptr;
					}
					else
					{
						if (prevtmp == cur)
							prevtmp->_right = tmp->_right;
						else
							prevtmp->_left = tmp->_right;
						delete tmp;
						tmp = nullptr;
					}
					return true;
				}
			}
		}
		return false;
	}

	

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	bool findR(const K& key)//递归版本的find
	{
		_findR(_root, key);
	}

	bool insertR(const K& key)
	{
		return _insertR(_root, key);
	}

	bool eraseR(const K& key)
	{
		return _eraseR(_root, key);
	}

	void inorder()//中序遍历:遍历完也就是有序的
	{
		_inorder(_root);
	}
private:
	void _inorder(Node* root)//中序:排序＋去重
	{
		if (root == nullptr)
			return;
		_inorder(root->_left);
		cout << root->_key << " ";
		_inorder(root->_right);
	}
	bool _findR(Node* root, const K& key)
	{
		if (root == nullptr) return false;
		if (root->_key > key) return _findR(root->_right, key);
		else if (root->_key < key) return _findR(root->_left, key);
		else if (root->_key == key) return true;
	}
	bool _insertR(Node*& root, const K& key)//这里加上引用后,就解决了需要穿父亲节点和当前节点的问题,root的值是空时
	{                                      //同时也是它父亲右/左指针的别名,将此空开辟一个空间后,指向不变,内容从空指针变成了key
		if (root == nullptr)
		{
			root = new Node(key);
			return true;
		}
		if (root->_key < key) return _insertR(root->_right, key);
		else if (root->_key > key) return _insertR(root->_left, key);
		else return false;
	}
	bool _eraseR(Node*& root, const K& key)
	{
		if (root == nullptr) return false;
		if (root->_key < key) return _eraseR(root->_right, key);
		else if (root->_key > key) return _eraseR(root->_left, key);
		else//开始删除
		{
			Node* tmp = root;
			if (root->_right == nullptr) root = root->_left;//这里直接让父亲节点指向当前节点的下一个节点,再将当前节点删除掉
			else if (root->_left == nullptr) root = root->_right;
			else//找右树最左边的节点来替换
			{
				Node* min = root->_right;
				while (min->_left)
					min = min->_left;
				swap(root->_key, min->_key);
				return _eraseR(root->_right, key);//交换后在当前节点的右子树递归删除key,若以左子树的最大值为基准,则要去当前节点的左子树删除key
			}
			delete tmp;
			tmp = nullptr;
			return true;
		}
	}
	void _destory(Node* root)
	{
		if (root == nullptr) return;
		_destory(root->_left);
		_destory(root->_right);
		delete root;
		root = nullptr;
	}
	Node* _copy(Node* root)//递归拷贝二叉树
	{
		if (root == nullptr) return nullptr;
		Node* copyroot = new Node(root->_key);
		copyroot->_left = _copy(root->_left);
		copyroot->_right = _copy(root->_right);
		return copyroot;
	}
private:
	Node* _root = nullptr;
};


void test1()
{
	BSTree<int> t;
	int a[] = { 8, 3, 1, 10, 6, 4, 7, 14, 13 };
	for (auto e : a)
		t.insert(e);
	t.inorder();
	cout << endl;
	t.erase(8);
	t.inorder();
	cout << endl;
	t.erase(3);
	t.inorder();
	cout << endl;
	t.erase(1);
	t.inorder();
	cout << endl;
	t.erase(10);
	t.inorder();
	cout << endl;
	t.erase(14);
	t.inorder();
	cout << endl;
	t.erase(7);
	t.inorder();
	cout << endl;
	t.erase(13);
	t.inorder();
	cout << endl;
	t.erase(6);
	t.inorder();
	cout << endl;
	t.erase(4);
	t.inorder();
	cout << endl;
}

void test2()
{
	BSTree<int> t;
	int a[] = { 8, 3, 1, 10, 6, 4, 7, 14, 13 };
	for (auto e : a)
		t.insert(e);
	t.inorder();
	cout << endl;
	t.eraseR(14);
	t.inorder();
	cout << endl;
	t.eraseR(3);
	t.inorder();
	cout << endl;
	t.eraseR(8);
	t.inorder();
	cout << endl;
	t.eraseR(1);
	t.inorder();
	cout << endl;
	t.eraseR(13);
	t.inorder();
	cout << endl;
	t.eraseR(6);
	t.inorder();
	cout << endl;
	t.eraseR(10);
	t.inorder();
	cout << endl;
	t.eraseR(4);
	t.inorder();
	cout << endl;
	t.eraseR(7);
	t.inorder();
	cout << endl;
}

void test3()
{
	BSTree<int> t;
	int a[] = { 8, 3, 1, 10, 6, 4, 7, 14, 13 };
	for (auto e : a)
		t.insert(e);
	BSTree<int> copy = t;
	copy.inorder();
}