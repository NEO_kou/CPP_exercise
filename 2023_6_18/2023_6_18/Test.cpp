#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable:6031)

#include<iostream>
using namespace std;

class Date
{
public:
	Date(int year = 1, int month = 1, int day = 1)//构造函数
	{
		_year = year;
		_month = month;
		_day = day;
	}

	int GetMonthDay(int year, int month)
	{
		static int day[] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };//将数组放在静态区,调用次数多了也不会很消耗栈帧

		if (year % 4 == 0 && year % 100 != 0&&month==2)//这里不能单纯是闰年就把day[2]改了.下次再调用day[2]就不是默认的28了而是29.
		{
			return 29;
		}
		else
		{
			return day[month];
		}
		
	}

	bool operator==(Date& d)//判断相同函数
	{
		return _year == d._year
			&& _month == d._month
			&& _day == d._day;
	}

	Date& operator+=(int day)//这里实现的功能是+=,因为d1本身的值改变了.并且这里用传引用返回?
	{
		_day += day;
		while (_day > GetMonthDay(_year,_month))
		{
			_day -= GetMonthDay(_year, _month);
			_month++;
			if (_month == 13)
			{
				_month = 1;
				_year++;
			}
		}
		return *this;//this为指向当前对象的指针,*this就是日期类本身
	}

	Date operator+(int day)//这个才是不会改变当前日期类d1的函数
	{
		Date dd(*this);//拷贝构造的场景
		dd._day += day;
		while (dd._day > GetMonthDay(dd._year, dd._month))
		{
			dd._day -= GetMonthDay(dd._year, dd._month);
			dd._month++;
			if (dd._month == 13)
			{
				dd._month = 1;
				dd._year++;
			}
		}
		return dd;
	}
private:
	int _year;
	int _month;
	int _day;
};



int main()
{
	Date d1(2023, 6, 18);
	//Date d2(d1);
	//cout << (d1 == d2) << endl;

	Date d3 = d1 + 100;
	return 0;
}