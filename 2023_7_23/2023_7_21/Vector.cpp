#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable:6031)

#include"vector.h"

template<class T>
typename Vector<T>::iterator Vector<T>::insert(typename Vector<T>::iterator pos, const T& x)//要加上typename,不然编译器无法识别类的内嵌类型:iterator是类型还是变量                         
{
	assert(pos >= _start && pos <= _finish);
	if (_finish == _endofsto)
	{
		size_t len = pos - _start;
		reserve(capacity() == 0 ? 4 : 2 * capacity());
		pos = _start + len;
	}
	iterator end = _finish - 1;
	while (end >= pos)
	{
		*(end + 1) = *end;
		end--;
	}
	*pos = x;
	_finish++;
	return pos;
}