#pragma once
#include<iostream>
#include<vector>
#include<assert.h>
#include<list>

using namespace std;

template<class T>
class Vector
{
public:
	typedef T* iterator;
	typedef const T* const_iterator;

	void swap(Vector<T>& v)
	{
		std::swap(_start, v._start);
		std::swap(_finish, v._finish);
		std::swap(_endofsto, v._endofsto);
	}

	iterator begin()
	{
		return _start;
	}

	const_iterator begin()const
	{
		return _start;
	}

	iterator end()
	{
		return _finish;
	}

	const_iterator end()const
	{
		return _finish;
	}

	Vector()
		:_start(nullptr)
		, _finish(nullptr)
		, _endofsto(nullptr)
	{

	}

	template<class InputIterator>
	Vector(InputIterator first, InputIterator last)//有迭代器区间的构造
		:_start(nullptr)
		, _finish(nullptr)
		, _endofsto(nullptr)
	{
		while (first != last)
		{
			push_back(*first);
			first++;
		}
	}

	//这里将size_t改为int可以解决间接寻址的错误,但是库中使用的是size_t
	Vector(int n, const T& val = T())//构造函数:插入n个val,T()是匿名对象的缺省,调用默认构造函数
		:_start(nullptr)
		, _finish(nullptr)
		, _endofsto(nullptr)
	{
		reserve(n);
		for (size_t i = 0; i < n; i++)
		{
			push_back(val);
		}
	}

	~Vector()
	{
		delete[] _start;
		_start = _finish = _endofsto = nullptr;
	}

	/*Vector(const Vector<T>& v)
		:_start(nullptr)
		,_finish(nullptr)
		,_endofsto(nullptr)
	{
		reserve(v.size());
		for (const auto& e : v)
		{
			push_back(e);
		}
	}*/

	Vector(const Vector<T>& v)
	{
		assert(v._start && v._finish && v._endofsto);
		_start = new T[v.capacity()];//给size或capacity都可以
		//memcpy(_start, v._start, sizeof(T) * v.size()); //使用memcpy时,数组是二维数组会发生问题
		for (size_t i = 0; i < size(); i++)
		{
			_start[i] = v._start[i];
			_finish = _start + v.size();
		}
		_endofsto = _start + v.capacity();
	}

	size_t capacity()const
	{
		return _endofsto - _start;
	}

	size_t size()const
	{
		return _finish - _start;
	}

	T& operator[](size_t pos)
	{
		assert(pos < size());
		return _start[pos];
	}

	const T& operator[](size_t pos)const
	{
		assert(pos < size());
		return _start[pos];
	}

	//void reserve(size_t n)
	//{
	//	if (n > capacity())
	//	{
	//		size_t sz = size();
	//		T* tmp = new T[n];
	//		if (_start)
	//		{
	//			memcpy(tmp, _start, sizeof(T) * size());
	//			delete[] _start;
	//		}
	//		_start = tmp;
	//		_finish = _start + sz;
	//		_endofsto = _start + n;//开辟n个空间
	//	}
	//}

	void reserve(size_t n)
	{
		if (n > capacity())
		{
			size_t sz = size();
			T* tmp = new T[n];
			if (_start)
			{
				//memcpy(tmp, _start, sizeof(T)*sz);
				for (size_t i = 0; i < sz; ++i)
				{
					tmp[i] = _start[i];
				}
				delete[] _start;
			}

			_start = tmp;
			_finish = _start + sz;
			_endofsto = _start + n;
		}
	}


	void resize(size_t n, const T& val = T())
	{
		if (n > capacity())//扩容
		{
			reserve(n);
		}
		if (n > size())//初始化,前面的数据不动,后面添加val
		{
			while (_finish < _start + n)
			{
				*_finish = val;
				_finish++;
			}
		}
		else if (n <= size())//删除数据,但不改变原先的数据
		{
			_finish = _start + n;
		}
	}

	void push_back(const T& x)
	{
		/*if (_endofsto == _finish)
		{
			reserve(capacity() == 0 ? 4 : 2 * capacity());
		}
		*_finish = x;
		_finish++;*/
		insert(end(), x);
	}

	void pop_back()
	{
		assert(_finish > _start);
		_finish--;
	}

	iterator insert(iterator pos, const T& x);//若不传入地址或引用,发生扩容后,p位置将不在start和finish之间                               
	//{                                    //虽然在函数内部修改了pos,但是pos只是p的一份拷贝
	//	assert(pos >= _start && pos <= _finish); //库中没有传地址或引用,按照库中的来
	//	if (_finish == _endofsto)
	//	{
	//		size_t len = pos - _start;
	//		reserve(capacity() == 0 ? 4 : 2 * capacity());
	//		pos = _start + len;//不更新pos位置的话,扩容后,迭代器会失效
	//	}
	//	iterator end = _finish - 1;
	//	while (end >= pos)
	//	{
	//		*(end + 1) = *end;
	//		end--;
	//	}
	//	*pos = x;
	//	_finish++;
	//	return pos;
	//}

	iterator erase(iterator pos)
	{
		assert(pos >= _start && pos < _finish);
		iterator end = pos;
		while (end <= _finish)
		{
			*(end) = *(end + 1);
			end++;
		}
		_finish--;
		return pos;

		//if (size() < capacity() / 2)//库中可能是这样实现的,缩容可能会有迭代器失效的问题
		//{
		//	//缩容
		//}
	}

	Vector<T>& operator=(Vector<T> v)//要交换this和v就不能传引用
	{
		swap(v);//交换后,局部变量v销毁时刚好把*this原先的空间给释放掉了
		return *this;
	}

	T& front()
	{
		assert(size() > 0);
		return *_start;
	}
	T& back()
	{
		assert(size() > 0);
		return *_finish;
	}

private:
	iterator _start;
	iterator _finish;
	iterator _endofsto;
};
