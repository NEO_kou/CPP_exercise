#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable:6031)
#include<iostream>
#include<array>
using namespace std;
//非类型模板参数,是常量
//template<class T,size_t N = 10>
//class test
//{
//
//private:
//	T _a[N];
//};
//
//int main()
//{
//	test<int, 50> c;
//	test<double> cc;
//	//int n = 10;
//	//array<int, n> a1;//不能传变量,只能用常量
//	array<int, 10> a1;
//	int a2[10];
//	a1[10];
//	//a2[10];
//	return 0;
//}
//


//模板的特化
//struct Date
//{
//	Date(int year, int month, int day)
//		:_year(year)
//		, _month(month)
//		, _day(day)
//	{}
//
//	bool operator>(const Date& d) const
//	{
//		if ((_year > d._year)
//			|| (_year == d._year && _month > d._month)
//			|| (_year == d._year && _month == d._month && _day > d._day))
//		{
//			return true;
//		}
//		else
//		{
//			return false;
//		}
//	}
//
//	bool operator<(const Date& d) const
//	{
//		if ((_year < d._year)
//			|| (_year == d._year && _month < d._month)
//			|| (_year == d._year && _month == d._month && _day < d._day))
//		{
//			return true;
//		}
//		else
//		{
//			return false;
//		}
//	}
//
//	int _year;
//	int _month;
//	int _day;
//};

//// 函数模板 -- 参数匹配
//template<class T>
//bool Greater(T left, T right)
//{
//	return left > right;
//}
//
//template<>//若传指针进来,就会走这条语句
//bool Greater<Date*>(Date* left, Date* right)
//{
//	return *left > *right;
//}
//
//template<class T>
//struct Less
//{
//	bool operator()(const T& x1, const T& x2) const
//	{
//		return x1 < x2;
//	}
//};
//
//template<class T>
//struct Less<T*>//偏特化,参数的进一步限制
//{
//	bool operator()(const T& x1, const T& x2) const
//	{
//		return x1 < x2;
//	}
//};
//
//template<>
//struct Less<Date*>//类的特化
//{
//	bool operator()(Date* x1, Date* x2) const
//	{
//		return *x1 < *x2;
//	}
//};
//
//int main()
//{
//	cout << Greater(1, 2) << endl; // 可以比较，结果正确
//	Date d1(2022, 7, 7);
//	Date d2(2022, 7, 8);
//	cout << Greater(d1, d2) << endl; // 可以比较，结果正确
//	Date* p1 = &d1;
//	Date* p2 = &d2;
//	cout << Greater(p1, p2) << endl; // 可以比较，结果错误
//
//	Less<Date> lessfunc1;//仿函数
//	cout << lessfunc1(d1, d2) << endl;
//	Less<Date*> lessfunc2;
//	cout << lessfunc2(p1, p2) << endl;//可以比较, 结果错误
//	return 0;
//}


//偏特化实例
//template<class T1,class T2>
//class Date
//{
//public:
//	Date()
//	{
//		cout << "T1 & T2" << endl;
//	}
//};
//
//template<class T1, class T2>
//class Date<T1*,T2*>//偏特化,参数的进一步限制
//{
//public:
//	Date()
//	{
//		cout << "T1* & T2*" << endl;
//	}
//};
//
//template<class T1>//只写一个 T1
//class Date<T1,int>//偏特化,部分特化
//{
//public:
//	Date()
//	{
//		cout << "T1 & int" << endl;
//	}
//};
//
//template<class T1, class T2>
//class Date<T1&,T2&>
//{
//public:
//	Date()
//	{
//		cout << "T1& & T2&" << endl;
//	}
//};
//
//template<class T1, class T2>
//class Date<T1&, T2*>
//{
//public:
//	Date()
//	{
//		cout << "T1& & T2*" << endl;
//	}
//};
//
//template<class T1, class T2>
//class Date<T1*, T2&>
//{
//public:
//	Date()
//	{
//		cout << "T1* & T2&" << endl;
//	}
//};
//
//int main()
//{
//	Date<int, int> d1;
//	Date<int*, int*> d2;
//	Date<int, double> d3;
//	Date<int&, char&> d4;
//	Date<int*, double&> d5;
//	Date<char&, int*> d6;
//	return 0;
//}


#include"vector.h"

int main()
{
	Vector<int> vv;
	vv.push_back(1);
	vv.push_back(2);
	vv.push_back(3);
	vv.push_back(4);


	return 0;
}