class Solution {
public:
    vector<vector<int>> threeSum(vector<int>& nums) {
        int n = nums.size();
        sort(nums.begin(), nums.end());
        vector<vector<int>> ret;
        for (int i = 0; i <= n - 3; i++)
        {
            int left = i + 1;
            int right = n - 1;
            while (left < right)
            {
                if (nums[left] + nums[right] + nums[i] == 0)
                {
                    vector<int> tmp;
                    tmp.push_back(nums[left]);
                    tmp.push_back(nums[right]);
                    tmp.push_back(nums[i]);
                    ret.push_back(tmp);
                    while (left < right && nums[left] == nums[left + 1])
                        left++;
                    while (left < right && nums[right] == nums[right - 1])
                        right--;
                    left++; right--;
                }
                else if (nums[left] + nums[right] + nums[i] > 0)
                    right--;
                else if (nums[left] + nums[right] + nums[i] < 0)
                    left++;
            }
            while (i < n - 3 && nums[i] == nums[i + 1]) i++;
        }
        return ret;
    }
};