#include<iostream>
#include<cstdlib>
#include<cstdio>
#include<cassert>
using namespace std;

//��source���ַ���������dest��
char* MyStrcpy(char* destination, const char* source)
{
	assert(destination);
	assert(source);
	char* ret = destination;
	int i = 0;
	while (source[i] != '\0')
	{
		destination[i] = source[i];
		i++;
	}
	destination[i] = '\0';
	return ret;
}

char* MyStrcat(char* destination, const char* source)
{
	assert(destination != NULL);
	assert(source != NULL);
	char* ret = destination;
	while (*destination != '\0')
	{
		assert(destination != source);
		destination++;
	}
	while (*source != '\0')
	{
		*destination = *source;
		destination++;
		source++;
	}
	*destination = *source;
	return ret;
}

int MyStrcmp(const char* str1, const char* str2)
{
	assert(str1);
	assert(str2);
	while (*str1 && *str2)
	{
		if (*str1 > *str2)
			return 1;
		else if (*str1 < *str2)
			return -1;
		str1++;
		str2++;
	}
	if(*str1=='\0'&&*str2=='\0')
		return 0;
	else
	{
		return *str1 == '\0' ? -1 : 1;
	}
}

int main()
{
	char a[100] = "helloworld";
	char b[] = "helloworld123";
	cout << MyStrcmp(a, b) << endl;
	//cout << a << endl << b << endl;
	return 0;
}