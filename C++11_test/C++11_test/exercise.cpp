#include<iostream>
#include<string>
#include<vector>
#include<list>
#include<functional>
using namespace std;

struct Point
{
	int _x;
	int _y;
};
class Date
{
public:
	Date(int year, int month, int day)
		:_year(year)
		, _month(month)
		, _day(day)
	{
		cout << "Date(int year, int month, int day)" << endl;
	}
private:
	int _year;
	int _month;
	int _day;
};
int main()
{
	//int j{ 10 };
	//int array1[] = { 1, 2, 3, 4, 5 };
	//int array2[5] = { 0 };
	//Point p = { 1, 2 };
	//Date d1(2022, 1, 1); // old style
	//// C++11支持的列表初始化，这里会调用构造函数初始化
	//Date d2{ 2022, 1, 2 };
	//Date d3 = { 2022, 1, 3 };///隐式类型转换,没有被识别为initializer_list
	//Date* p1 = new Date[3]{ {2023,11,30},{2023,11,31},{2023,11,32} };

	////vector和list使用初始化列表可以写任意个值,然而Date类或整型的自定义类型只能写符合构造函数参数个数的值
	//vector<int> v1{ 1,2,3,4,5,6,7 };

	auto it = { 1,2,3,4 };//li是initializer_list类型
	cout << typeid(it).name() << endl;
	return 0;
}