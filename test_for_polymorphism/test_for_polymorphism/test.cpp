#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable:6031)

#include<iostream>
using namespace std;
class A
{
public:
	void func1()
	{
		cout << "����func1";
	}
	void func2()
	{
		cout << "����func2";
	}
private:
	int _a;
};
class B : public A
{
public:
	virtual void func1()
	{
		cout << "����func1";
	}
private:
	int _b;
};

int main()
{
	A a;
	B b;
	return 0;
}