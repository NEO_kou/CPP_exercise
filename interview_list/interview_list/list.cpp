#include<iostream>
#include<assert.h>
using namespace std;
class ListNode
{
public:
	int _data;
	ListNode* _next;
	ListNode* _prev;
	ListNode(int data = 0)
		:_data(data),_next(nullptr),_prev(nullptr)
	{}
};
class Iterator
{
	typedef ListNode node;
public:
	node* _node;
	Iterator(node* node):_node(node)
	{}
	Iterator operator++()
	{
		_node = _node->_next;
		return *this;
	}
	Iterator operator++(int)
	{
		Iterator ret = *this;
		_node = _node->_next;
		return ret;
	}
	Iterator operator--()
	{
		_node = _node->_prev;
		return *this;
	}
	Iterator operator--(int)
	{
		Iterator ret = *this;
		_node = _node->_prev;
		return ret;
	}
	bool operator!=(Iterator it)
	{
		return _node != it._node;
	}
	int operator*()
	{
		return _node->_data;
	}
	int* operator->()
	{
		return &(_node->_data);
	}
};
class List
{
	typedef Iterator iterator;
	typedef ListNode node;
public:
	node* _head;
public:
	iterator begin()
	{
		return iterator(_head->_next);
	}
	iterator end()
	{
		return iterator(_head);
	}
	List()
	{
		_head = new node;
		_head->_next = _head;
		_head->_prev = _head;
	}
	~List()
	{
		node* cur = _head->_next;
		while (cur != _head)
		{
			node* prev = cur->_prev;
			node* next = cur->_next;
			prev->_next = next;
			next->_prev = prev;
			delete cur;
			cur = next;
		}
		delete _head;
	}
	List(const List& l)
	{
		_head = new node;
		_head->_next = _head;
		_head->_prev = _head;
		node* cur = l._head->_next;
		while (cur != l._head)
		{
			PushBack(cur->_data);
			cur = cur->_next;
		}

	}
	void Insert(iterator pos, int val)
	{
		node* cur = pos._node;
		node* newnode = new node(val);
		node* prev = cur->_prev;
		prev->_next = newnode;
		newnode->_prev = prev;
		newnode->_next = cur;
		cur->_prev = newnode;
	}
	void Erase(iterator pos)
	{
		assert(pos._node != _head);
		node* cur = pos._node;
		node* prev = cur->_prev;
		node* next = cur->_next;
		prev->_next = next;
		next->_prev = prev;
		delete cur;
	}
	void PushBack(int val)
	{
		Insert(end(), val);
	}
};

int main()
{
	List list1;
	list1.PushBack(1);
	list1.PushBack(2);
	list1.PushBack(3);
	list1.PushBack(4);
	list1.PushBack(5);
	list1.PushBack(6);
	auto it = list1.begin();
	while (it != list1.end())
	{
		cout << *it << " ";
		it++;
	}
	return 0;
}