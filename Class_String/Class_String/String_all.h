#pragma once

#include<iostream>
#include<assert.h>
#include<string.h>
using namespace std;


//模拟实现string类
class String
{
public:
	typedef char* iterator; //重命名迭代器
	typedef const char* const_iterator;//const迭代器
	//String()
	//	:_str(new char[1])
	//	, _size(0)
	//	, _capacity(0)
	//{
	//	_str[0] = '\0';
	//}

	String(const char* str = "\0")
		:_size(strlen(str))
		, _capacity(_size)
		, _str(new char[_capacity + 1])
	{
		strcpy(_str, str);
		_str[_size] = '\0';
	}

	~String()
	{
		delete[] _str;
		_str = nullptr;
		_size = _capacity = 0;
	}

	//传统写法---老老实实
	//String(const String& s) //拷贝构造,深拷贝
	//	:_str(new char[s._capacity])
	//	,_size(s._size)
	//	,_capacity(s._capacity)
	//{
	//	strcpy(_str, s._str);
	//}

	//现代写法---老板思维
	void swap(String& s)
	{
		::swap(_str, s._str);//内部实现的swap是库函数中的swap
		::swap(_size, s._size);
		::swap(_capacity, s._capacity);
	}

	String(const String& s)
	{
		String tmp(s._str);//调用了构造函数
		//this->swap(tmp);  //本质上是this和tmp交换
		swap(tmp);
	}

	const char* c_str() const
	{
		return _str;
	}

	size_t size()const//此函数只读,所以返回不用const
	{
		return _size;
	}

	const char& operator[](size_t pos) const//const对象返回后也应该是const
	{
		assert(pos < _size);
		return _str[pos];
	}

	char& operator[](size_t pos)
	{
		assert(pos < _size);
		return _str[pos];
	}

	void resize(size_t n, char ch = '\0')//开辟空间＋初始化
	{
		if (n > _size)//插入数据(在原先内容上插入)
		{
			reserve(n);
			for (size_t i = _size; i < n; i++)
			{
				_str[i] = ch;
			}
			_str[n] = '\0';
			_size = n;
		}
		else//删除数据
		{
			_str[n] = '\0';
			_size = n;
		}
	}

	iterator begin()//返回第一个位置的指针
	{
		return _str;
	}

	iterator end()//返回最后一个位置的下一个位置的指针
	{
		return _str + _size;
	}

	const_iterator begin()const//返回const类型
	{
		return _str;
	}

	const_iterator end()const//迭代器能读能写
	{
		return _str + _size;
	}

	//传统写法
	//String& operator=(const String& s)
	//{
	//	if (this == &s)
	//	{
	//		return *this;
	//	}
	//	delete[] _str;
	//	_str = new char[s._capacity+1];//多开辟一个空间给'\0'做准备
	//	_size = s._size;
	//	_capacity = s._capacity;
	//	strcpy(_str, s._str);
	//	return *this;
	//}

	//现代写法
	String& operator=(const String& s)
	{
		if (this == &s)
		{
			return *this;
		}
		//String tmp(s);//调用拷贝构造
		String tmp(s._str);//调用构造
		swap(tmp);//tmp和this交换后,tmp还会帮this把以前的空间给销毁掉,天选打工人!
		return *this;
	}

	void reserve(size_t n)//可以完成扩容工作
	{
		if (n > _capacity)
		{
			char* tmp = new char[n + 1];
			strcpy(tmp, _str);
			delete[] _str;
			_str = tmp;
			_capacity = n;
		}
	}

	void push_buck(char ch)
	{
		if (_size >= _capacity)
		{
			reserve(_capacity == 0 ? 4 : 2 * _capacity);
		}
		_str[_size] = ch;
		_size++;
		_str[_size] = '\0';  //处理\0很容易忘记!
	}

	void append(const char* str)
	{
		size_t len = strlen(str);
		if (_size + len > _capacity)
		{
			reserve(_size + len);
		}
		strcpy(_str + _size, str);
		_size += len;
	}

	String& operator+=(char ch)
	{
		push_buck(ch);
		return *this;
	}

	String& operator+= (const char* str)
	{
		append(str);
		return *this;
	}

	String& operator+=(const String& s)
	{
		append(s._str);
		return *this;
	}

	String& insert(size_t pos, char ch)
	{
		assert(pos <= _size);
		if (_size >= _capacity)
		{
			reserve(_capacity == 0 ? 4 : 2 * _capacity);
		}
		size_t end = _size + 1;
		while (end < pos)
		{
			_str[end] = _str[end - 1];
			end--;
		}
		_str[pos] = ch;
		_size++;
		return *this;
	}

	String& insert(size_t pos, char* str)//pos位置后插入
	{
		assert(pos <= _size);
		size_t len = strlen(str);
		if (_size + len > _capacity)
		{
			reserve(_size + len);
		}
		size_t end = _size + len;
		while (end >= pos + len)
		{
			_str[end] = _str[end - len];
			end--;
		}
		strncpy(_str + pos, str, len);
		_size += len;
		return *this;
	}

	void erase(size_t pos = 0, size_t len = npos)
	{
		assert(pos < _size);
		if (len == npos || pos + len >= _size)
		{
			_str[pos] = '\0';
			_size = pos;
		}
		else
		{
			size_t end = pos + len;
			while (end <= _size)
			{
				_str[end - len] = _str[end];
				end++;
			}
			// strcpy(_str + len, str + len + pos) ;
			_size -= len;
		}
	}

	size_t find(char ch, size_t pos = 0)const
	{
		assert(pos < _size);
		for (int i = pos; i < _size; i++)
		{
			if (_str[i] == ch)
			{
				return i;
			}
		}
		return npos;
	}

	size_t find(char* sub, size_t pos = 0)const
	{
		assert(sub);
		assert(pos < _size);
		int len = strlen(sub);
		for (int i = pos; i <= _size - len; i++)
		{
			int flag = 0;
			int j = 0;
			int tmp = i;
			while (j < len)
			{
				if (_str[tmp] != sub[j])
				{
					flag = 1;
					break;
				}
				tmp++;
				j++;
			}
			if (flag == 0)
			{
				return i;
			}
		}
		return npos;
	}

	String substr(size_t pos, size_t len = npos)const
	{
		assert(pos < _size);
		String tmp;
		size_t real_len = len;
		if (len == npos || len > _size - pos)
		{
			real_len = _size - pos;
		}
		for (size_t i = 0; i < real_len; i++)
		{
			tmp += _str[pos + i];
		}
		return tmp;
	}

	bool operator==(const String& s)const
	{
		return strcmp(_str, s._str) == 0;
	}

	bool operator>=(const String& s)const
	{
		return *this > s || *this == s;
	}

	bool operator<=(const String& s)const
	{
		return *this < s || *this == s;
	}

	bool operator<(const String& s)const
	{
		return strcmp(_str, s._str) < 0;
	}

	bool operator>(const String& s)const
	{
		return strcmp(_str, s._str) > 0;
	}

	bool operator!=(const String& s)const
	{
		return !(*this == s);
	}

private:
	size_t _size;
	size_t _capacity;
	char* _str;
	static size_t npos;
};

size_t String::npos = -1;

ostream& operator<<(ostream& out, const String& s)
{
	for (size_t i = 0; i < s.size(); i++)
	{
		out << s[i];
	}
	return out;
}

istream& operator>>(istream& in, String& s)
{
	char ch;
	ch = in.get();
	while (ch != ' ' || ch != '\n')
	{
		s += ch;
		ch = in.get();
	}
	return in;
}
