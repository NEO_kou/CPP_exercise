#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable:6031)

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<stdbool.h>

//char* convertToTitle(int columnNumber)//力扣168
//{
//    char a[] = { 'Z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z' };
//    char* p = (char*)malloc(sizeof(char) * 8);
//    int i = 0;
//    while (columnNumber > 0)
//    {
//        int x = columnNumber % 26;
//        if (x == 0)
//        {
//            columnNumber -= 1;
//        }
//        p[i++] = a[x];
//        columnNumber -= x;
//        columnNumber /= 26;
//    }
//    p[i] = '\0';
//    for (int j = 0; j < i / 2; ++j) {
//        int tmp = p[j];
//        p[j] = p[i - 1 - j];
//        p[i - 1 - j] = tmp;
//    }
//    return p;
//}
//
//int majorityElement(int* nums, int numsSize) {//力扣169.总有一个数的数量大于n/2.采用抵消法,最坏的情况也可以剩下一个数
//    int m = 0, cnt = 0;
//    for (int i = 0; i < numsSize; ++i)
//    {
//        if (cnt == 0)
//            m = nums[i];
//        if (m == nums[i])
//            cnt++;
//        else
//            cnt--;//值不对应就抵消
//    }
//    return m;
//}
//
//int hammingWeight(uint32_t n)//力扣191
//{
//    int count = 0;
//    for (int i = 0; i < 32; i++)
//    {
//        if (n & 1 == 1)
//        {
//            count++;
//        }
//        n = n >> 1;
//    }
//    return count;
//}



bool isHappy(int n)//力扣202.快乐数
{
    int count = 0;
    int x = n;
    while (count < 1000)
    {
        int sum = 0;
        while (x > 0)
        {
            int a = x % 10;
            sum += pow(a, 2);
            x /= 10;
        }
        if (sum == 1)
        {
            return true;
        }
        x = sum;
        count++;
    }
    return false;
}


int main()
{
    bool xx = isHappy(19);

    printf("%d", xx);
    return 0;
}