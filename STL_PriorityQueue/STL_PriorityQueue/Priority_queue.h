#pragma once
#include<vector>
#include<iostream>
#include<algorithm>
#include<iostream>

using namespace std;

//大堆
template<class T, class Container = vector<T>, class Compare = std::less<T>>//compare是用于进行比较的仿函数.如果是less就是大堆,如果是greater就算小堆
class Priority_queue
{
public:

	template<class InputIterator>
	Priority_queue(InputIterator first, InputIterator last)//用迭代器区间构造
	{
		while (first != last)//先全部push进去
		{
			_con.push_back(*first);
			first++;
		}
		//再用此数组建堆
		for (int i = (_con.size() - 1 - 1) / 2; i >= 0; i--)
		{
			adjust_down(i);
		}
	}

	Priority_queue()//在初始化阶段会在初始化列表调用它的默认构造
	{

	}
	void adjust_up(size_t child)
	{
		Compare com;//com是用于使用仿函数的对象
		size_t parent = (child - 1) / 2;
		while (child > 0)
		{
			//if (_con[child] >= _con(parent))
			if(com(_con[parent],_con[child]))//因为less的底层实现是return x1<x2,这句话为真返回true,证明con[parent]<con[chld],刚好满足大堆的交换条件,这里的parent和child顺序不能交换
			{
				std::swap(_con[parent], _con[child]);
				child = parent;
				parent = (child - 1) / 2;
			}
			else
			{
				break;
			}
		}
	}

	void adjust_down(size_t parent)
	{
		Compare com;
		size_t child = parent * 2 + 1;
		while (child < _con.size())
		{
			// 选出左右孩子中大的那一个
			//if (child+1 < _con.size() && _con[child+1] > _con[child])
			//if (child + 1 < _con.size() && _con[child] < _con[child + 1])
			if (child + 1 < _con.size() && com(_con[child], _con[child + 1]))
			{
				++child;
			}

			//if (_con[child] > _con[parent])
			//if (_con[parent] < _con[child])
			if (com(_con[parent], _con[child]))
			{
				std::swap(_con[child], _con[parent]);
				parent = child;
				child = parent * 2 + 1;
			}
			else
			{
				break;
			}
		}
	}


	void push(const T& val)//堆的插入
	{
		_con.push_back(val);
		adjust_up(_con.size() - 1);
	}

	void pop()//堆的删除
	{
		std::swap(_con[0], _con[_con.size() - 1]);
		_con.pop_back();
		adjust_down(0);
	}

	const T& top()
	{
		return _con[0];
	}

	bool empty()
	{
		return _con.empty();
	}

	size_t size()
	{
		return _con.size();
	}
private:
	Container _con;

};
