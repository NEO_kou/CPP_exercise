
#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable:6031)
#include<iostream>
#include<stack>
#include<queue>
#include<list>
#include<functional>
#include"Priority_queue.h"
using namespace std;

//void testS()
//{
//	stack<int> st;
//	st.push(1);
//	st.push(2);
//	st.push(3);
//	st.push(4);
//
//	while (!st.empty())
//	{
//		cout << st.top() << " ";
//		st.pop();
//	}
//}
//
//void testQ()
//{
//	queue<int> q;
//	q.push(1);
//	q.push(2);
//	q.push(3);
//	q.push(4);
//
//	while (!q.empty())
//	{
//		cout << q.front() << " ";
//		q.pop();
//	}
//}
//
//void test1()
//{
//	//Stack<int,vector<int>> st;//这里需要传两个模板参数
//	Stack<int, list<int>> st;//vector可以尾插尾删,list也可以做到,底层就可以使用list实现
//	st.push(1);
//	st.push(2);
//	st.push(3);
//	st.push(4);
//	st.push(5);
//
//	st.pop();
//	while (!st.empty())
//	{
//		cout << st.top() << " ";
//		st.pop();
//	}
//}
//
//void test2()
//{
//	//Queue<int,vector<int>> q; //vector 没有支持头删的接口(pop_front)
//	// 
//	//Queue<int, list<int>> q;  //list可以实现
//	Queue<int> q; //默认的适配器
//	q.push(1);
//	q.push(2);
//	q.push(3);
//	q.push(4);
//	q.push(5);
//
//	while (!q.empty())
//	{
//		cout << q.front() << " ";
//		q.pop();
//	}
//}

//void test3()
//{
//	/*priority_queue<int> pq;
//	pq.push(1);
//	pq.push(2);
//	pq.push(9);
//	pq.push(3);
//	pq.push(1);
//	pq.push(4);
//
//	while (!pq.empty())
//	{
//		cout << pq.top() << " ";
//		pq.pop();
//	}*/
//
//	int a[] = { 3,2,7,9,4,6,1,5,8 };
//
//	priority_queue<int,vector<int>,greater<int>> pq(a, a + 9);//迭代器区间
//
//	while (!pq.empty())
//	{
//		cout << pq.top() << " ";
//		pq.pop();
////	}
////}
//
void test4()
{
	priority_queue<int> pp(less<int>());
	Priority_queue<int, vector<int>,greater<int>> pq;
	pq.push(3);
	pq.push(1);
	pq.push(2);
	pq.push(5);
	pq.push(0);
	pq.push(1);//直接push进去的数组不一定有序,但是取一次top再删除一次得到的数据顺序一定是有序的,因为每次pop会向下调整

	while (!pq.empty())
	{
		cout << pq.top() << " ";
		pq.pop();
	}
}

template<class T>
struct Less
{
	bool operator()(const T& x1, const T& x2)const //重载(),仿函数
	{
		return x1 < x2;
	}
};

template<class T>
struct Greater
{
	bool operator()(const T& x1, const T& x2)const //重载(),仿函数
	{
		return x1 > x2;
	}
};


int main()
{
	test4();
	Less<int> neo;
	cout << endl;
	cout << neo(10, 20) << " ";//仿函数
	return 0;
}