#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable:6031)
#include<stdio.h>
#include<stdbool.h>
#include"../../StackCPPlib/StackCPPlib/Stack.h"


bool isValid(const char* s)
{
    ST st;
    StackInit(&st);
    int i = 0;
    while (s[i] != '\0')
    {
        if (s[i] == '(' || s[i] == '[' || s[i] == '{')
        {
            StackPush(&st, s[i]);
            i++;
        }
        else//遇见右括号了,但是栈里面可能没有数据,说明前面没有左括号
        {
            if (StackEmpty(&st))
            {
                StackDestroy(&st);
                return false;
            }
            STDataType top = StackTop(&st);
            StackPop(&st);
            if ((s[i] == ')' && top != '(') || (s[i] == ']' && top != '[') || (s[i] == '}' && top != '{'))
            {
                StackDestroy(&st);
                return false;
            }
            else
            {
                i++;
            }
        }
    }
    bool ret = StackEmpty(&st);//当栈里不为空时,证明还有左括号与右括号没有匹配
    StackDestroy(&st);
    return ret;

}


int main()
{
    printf("%d", isValid("{{{}}}"));
    return 0;

}