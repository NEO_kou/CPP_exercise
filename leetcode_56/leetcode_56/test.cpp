class Solution {
public:
    vector<vector<int>> merge(vector<vector<int>>& intervals) {
        sort(intervals.begin(), intervals.end(), [](vector<int> v1, vector<int> v2) {
            return v1[0] < v2[0];
            });
        vector<vector<int>> ret;
        int begin = intervals[0][0];
        int end = intervals[0][1];
        for (int i = 1; i < intervals.size(); i++)
        {
            if (intervals[i][0] <= end)
            {
                end = max(end, intervals[i][1]);
            }
            else
            {
                vector<int> tmp{ begin,end };
                ret.push_back(tmp);
                begin = intervals[i][0];
                end = intervals[i][1];
            }
        }
        vector<int> tmp{ begin,end };
        ret.push_back(tmp);
        return ret;
    }
};