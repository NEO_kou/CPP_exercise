class Solution {
public:
    int LastRemaining_Solution(int n, int m) {
        vector<int> v;
        for (int i = 0; i < n; i++)
            v.push_back(i);
        int index = 0;
        while (1)
        {
            if (v.size() == 1)
                break;
            index += m - 1;
            index %= v.size();
            if (v.begin() + index == v.end() - 1 || index == 0)
            {
                v.erase(v.begin() + index);
                index = 0;
            }
            else {
                v.erase(v.begin() + index);
            }
        }
        return v[0];
    }
};