﻿#include<iostream>
#include<vector>
#include<string>
#include<mutex>
#include<thread>
#include<cstdlib>
#include<time.h>
using namespace std;

////思路一,封构造
//class HeapOnly
//{
//public:
//	static HeapOnly* CreateObject(int x = 0)//先有对象才能调用成员函数,要有对象就要先调用此成员函数,套圈了,声明为static可以解决问题!
//	{
//		return new HeapOnly(x);
//	}
//private:
//	HeapOnly(int x = 0):_x(x)
//	{}
//	// C++98
//	// 1.只声明,不实现。因为实现可能会很麻烦，而你本身不需要
//	// 2.声明成私有
//	//HeapOnly(const HeapOnly&)；
//	
//		// C++11    
//	HeapOnly(const HeapOnly&) = delete;
//	int _x;
//};
//
////思路二,封析构
//class HeapOnly
//{
//public:
//	void destory()
//	{
//		delete this;
//	}
//private:
//	~HeapOnly()
//	{
//		cout << "!Heaponly" << endl;
//	}
//	int x;
//};
//
//class StackOnly
//{
//public:
//	static StackOnly CreateObj(int a = 0)
//	{
//		return StackOnly(a);
//	}
//
//	// 禁掉operator new可以把下面用new 调用拷贝构造申请对象给禁掉
// // StackOnly obj = StackOnly::CreateObj();
// // StackOnly* ptr3 = new StackOnly(obj);
//	void* operator new(size_t size) = delete;
//	void operator delete(void* p) = delete;
//private:
//	StackOnly(int a = 0)
//		:_a(a)
//	{}
//private:
//	int _a;
//};


//饿汉模式
class Singleton
{
public:
	static Singleton* GetInstance()
	{
		return _ins;
	}
	void Add(const string& str)
	{
		mtx.lock();
		_v.push_back(str);
		mtx.unlock();
	}
	void Print()
	{
		mtx.lock();
		for (auto x : _v)
			cout << x << " ";
		cout << endl;
		mtx.unlock();
	}
private:
	//限制类外随意创建对象
	Singleton(const Singleton& s) = delete;
	Singleton& operator=(const Singleton& s) = delete;
	Singleton()
	{}
private:
	mutex mtx;
	vector<string> _v;
	static Singleton* _ins;
};
Singleton* Singleton::_ins = new Singleton;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//懒汉模式
class Singleton
{
public:
	static Singleton* GetInstance()
	{
		if (_ins == nullptr)//双检查加锁,只有第一次进来时需要加锁,其他情况不用加锁
		{
			imtx.lock();
			if (_ins == nullptr)//第一次调用才创建实例!
			{
				_ins = new Singleton;
			}
			imtx.unlock();
		}
		
		return _ins;
	}
	void Add(const string& str)
	{
		mtx.lock();
		_v.push_back(str);
		mtx.unlock();
	}
	void Print()
	{
		mtx.lock();
		for (auto x : _v)
			cout << x << " ";
		cout << endl;
		mtx.unlock();
	}

	void DelInstance()
	{
		imtx.lock();
		if (_ins != nullptr)
		{
			cout << "over!!!" << endl;
			delete _ins;
			_ins = nullptr;
		}
		imtx.unlock();
	}
	class GC//若忘记调用Del函数,则可以很顺滑的使用此方法在main函数走完时去调用!
	{
	public:
		~GC()
		{
			Singleton().DelInstance();
		}
	};
private:
	//限制类外随意创建对象
	Singleton(const Singleton& s) = delete;
	Singleton& operator=(const Singleton& s) = delete;
	Singleton()
	{}
private:
	mutex mtx;
	vector<string> _v;
	static Singleton* _ins;
	static mutex imtx;
	static GC _gc;
};
Singleton* Singleton::_ins = nullptr;
mutex Singleton::imtx;
Singleton::GC Singleton::_gc;

int main()
{
	srand(time(nullptr));
	int n = 100;
	/*Singleton::GetInstance()->Add("abcdefg");
	Singleton::GetInstance()->Add("1234567");
	Singleton::GetInstance()->Add("!@#$%^&");
	Singleton::GetInstance()->Print();*/
	thread t1([n]()
		{
			for (int i = 0; i < n; i++)
			{
				Singleton::GetInstance()->Add("t1线程: " + to_string(rand()));
			}
		});
	thread t2([n]()
		{
			for (int i = 0; i < n; i++)
			{
				Singleton::GetInstance()->Add("t2线程: " + to_string(rand()));
			}
		});
	t1.join();
	t2.join();
	Singleton::GetInstance()->Print();


	return 0;
}