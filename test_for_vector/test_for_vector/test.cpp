#include<vector>
#include<iostream>
using namespace std;

int removeDuplicates(vector<int>& nums) {
    auto right = nums.begin();
    auto left = nums.begin();
    while (right != nums.end()) {
        if (*right == *left)
            right++;
        else {
            if (right - left > 2)
                left = nums.erase(left, left + (right - left - 2));
            right = left + 2;
            left = right;
        }
    }
    if (right - left > 2)
        nums.erase(left, left + (right - left - 2));
    return nums.size();
}

int main()
{
	vector<int> v{ 1,2,2,2};
    removeDuplicates(v);

	return 0;
}