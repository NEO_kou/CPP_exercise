#pragma once

#include<iostream>
using namespace std;
struct QueueNode
{
	QueueNode* next;
	int val;
};

// 
// 
class Queue
{
public:
	//inline void Init();
	void Init();
	void Push(int x);
	void Pop();
private:
	QueueNode* head;
	QueueNode* tail;
};
