class Solution {
public:
    vector<vector<string>> ret;
    vector<string> path;
    vector<bool> checkcol;
    vector<bool> dist1;
    vector<bool> dist2;
    void dfs(int n, int row)
    {
        if (row == n)
        {
            ret.push_back(path);
            return;
        }
        for (int col = 0; col < n; col++)
        {
            if (!checkcol[col] && !dist1[col - row + n] && !dist2[col + row])
            {
                path[row][col] = 'Q';
                checkcol[col] = dist1[col - row + n] = dist2[col + row] = true;
                dfs(n, row + 1);
                path[row][col] = '.';
                checkcol[col] = dist1[col - row + n] = dist2[col + row] = false;
            }

        }
    }
    vector<vector<string>> solveNQueens(int n) {
        checkcol.resize(10, false);
        dist1.resize(20, false);
        dist2.resize(20, false);
        path.resize(n);
        for (auto& str : path)
            str.resize(n, '.');
        dfs(n, 0);
        return ret;
    }
};