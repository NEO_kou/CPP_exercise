class Solution {
public:
    string longestCommonPrefix(vector<string>& strs) {
        string ret;
        int minlen = 0x3f3f3f;
        for (auto str : strs)
        {
            int tmplen = str.size();
            minlen = min(minlen, tmplen);
        }
        for (int i = 0; i < minlen; i++)
        {
            char p = strs[0][i];
            bool flag = false;
            for (auto str : strs)
            {
                if (str[i] != p)
                    flag = true;
            }
            if (flag == false)
                ret += p;
            else break;
        }
        return ret;

    }
};