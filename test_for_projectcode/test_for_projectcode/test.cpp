﻿#include<iostream>
#include<thread>
#include<mutex>
using namespace std;


size_t _RoundUp(size_t bytes, size_t align)
{
	return (((bytes)+align - 1) & ~(align - 1));
}
static inline size_t RoundUp(size_t bytes)
{
	if (bytes <= 128)
		return _RoundUp(bytes, 8);
	else if (bytes <= 1024)
		return _RoundUp(bytes, 16);
	else if (bytes <= 8 * 1024)
		return _RoundUp(bytes, 128);
	else if (bytes <= 64 * 1024) 
		return _RoundUp(bytes, 1024);
	else if (bytes <= 256 * 1024) 
		return _RoundUp(bytes, 8 * 1024);
	return -1;
}
int main()
{
	mutex _mtx;
	_mtx.lock();
	_mtx.unlock();
	_mtx.unlock();
	return 0;
}